# timetable

> **Note**
>
> Repository migrated from `github:voidedWarranties/timetable`.

An experimental task tracking program written in [osu!framework](https://github.com/ppy/osu-framework).

* Manrope (`timetable.Resources/Fonts`) is licensed under OFL-1.1.
* The icon (`timetable.Desktop/game.ico`) is from osu-framework, copyright ppy
  Pty Ltd, licensed under MIT.
