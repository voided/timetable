﻿using System;
using DiscordRPC;
using DiscordRPC.Message;
using osu.Framework.Allocation;
using osu.Framework.Graphics;
using osu.Framework.Logging;
using timetable.Game.Tasks.Stores;
using timetable.Game.Tasks.Types;

namespace timetable.Desktop
{
    public class DiscordRichPresence : Component
    {
        private const string client_id = "881307763559514122";

        private DiscordRpcClient client;
        private TaskStore store;

        [BackgroundDependencyLoader]
        private void load(TaskStore store)
        {
            this.store = store;

            client = new DiscordRpcClient(client_id)
            {
                SkipIdenticalPresence = false
            };

            client.OnReady += onReady;

            client.Initialize();

            store.ItemUpdated += updateStatus;
            store.ItemDeleted += updateStatus;
            store.TaskQueued += updateStatus;
            store.TaskStarted += updateStatus;
            store.TaskPaused += updateStatus;
            store.TaskEnded += updateStatus;
        }

        private void onReady(object sender, ReadyMessage e)
        {
            Logger.Log($"RPC Initialized, user: {e.User.Username}", LoggingTarget.Network);
            updateStatus();
        }

        private void updateStatus(Task _) => updateStatus();
        private void updateStatus(RunnableTask _) => updateStatus();

        private void updateStatus()
        {
            Assets assets;

            var presence = new RichPresence
            {
                Assets = assets = new Assets()
            };

            var task = store.FocusTask;

            if (task != null)
            {
                presence.Details = task.Group;
                presence.State = task.Name;

                var start = store.GetStart(task);

                if (start != null)
                {
                    var timestamp = ((DateTimeOffset)start.Value).ToUnixTimeMilliseconds();

                    presence.Timestamps = new Timestamps
                    {
                        StartUnixMilliseconds = (ulong)timestamp // using the date doesn't account for timezone. Lame!
                    };
                }

                assets.LargeImageKey = "work-red";
                assets.LargeImageText = "Working";

                assets.SmallImageKey = "unavailable";
                assets.SmallImageText = "Unavailable";
            }
            else
            {
                presence.Details = "Currently Free";

                assets.LargeImageKey = "logo";
                assets.LargeImageText = "timetable";

                assets.SmallImageKey = "free";
                assets.SmallImageText = "Free";
            }

            client.SetPresence(presence);
        }

        protected override void Dispose(bool isDisposing)
        {
            store.ItemUpdated -= updateStatus;
            store.ItemDeleted -= updateStatus;
            store.TaskQueued -= updateStatus;
            store.TaskStarted -= updateStatus;
            store.TaskPaused -= updateStatus;

            client.Dispose();

            base.Dispose(isDisposing);
        }
    }
}
