﻿using osu.Framework;
using osu.Framework.Platform;

namespace timetable.Desktop
{
    public static class Program
    {
        public static void Main()
        {
            using (GameHost host = Host.GetSuitableDesktopHost(@"timetable"))
            using (osu.Framework.Game game = new TimetableDesktopGame())
                host.Run(game);
        }
    }
}
