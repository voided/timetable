﻿using timetable.Game;

namespace timetable.Desktop
{
    internal class TimetableDesktopGame : TimetableGame
    {
        protected override void LoadComplete()
        {
            base.LoadComplete();

            LoadComponentAsync(new DiscordRichPresence(), Add);
        }
    }
}
