﻿using Newtonsoft.Json;

namespace timetable.Game.IO
{
    /// <summary>
    /// A store for loading and saving JSON data.
    /// </summary>
    public abstract class JsonStore<T>
        where T : new()
    {
        private readonly JsonSerializerSettings serializerSettings = new JsonSerializerSettings
        {
            NullValueHandling = NullValueHandling.Ignore,
            TypeNameHandling = TypeNameHandling.Auto
        };

        private T currentValue;

        public T Value
        {
            get => currentValue;
            set
            {
                currentValue = value;

                Save();
            }
        }

        public void Load()
        {
            var serialized = GetSerialized();

            if (string.IsNullOrEmpty(serialized))
            {
                currentValue = new T();
                return;
            }

            currentValue = JsonConvert.DeserializeObject<T>(serialized, serializerSettings);
        }

        protected string Serialize() => JsonConvert.SerializeObject(currentValue, serializerSettings);

        /// <summary>
        /// Gets the serialized JSON data from storage (e.g. file)
        /// </summary>
        protected abstract string GetSerialized();

        public abstract void Save();
    }
}
