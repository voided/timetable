﻿using System.IO;
using osu.Framework.Platform;

namespace timetable.Game.IO
{
    /// <summary>
    /// A <see cref="JsonStore{T}"/> that uses a file to store its data.
    /// </summary>
    public class FileJsonStore<T> : JsonStore<T>
        where T : new()
    {
        private const string file_name = @"store.json";

        private readonly Storage storage;

        public FileJsonStore(Storage storage)
        {
            this.storage = storage;

            Load();
        }

        protected override string GetSerialized() =>
            storage.Exists(file_name) ? File.ReadAllText(storage.GetFullPath(file_name)) : null;

        public override void Save()
        {
            var path = storage.GetFullPath(file_name, true);

            File.WriteAllText(path, Serialize());
        }
    }
}
