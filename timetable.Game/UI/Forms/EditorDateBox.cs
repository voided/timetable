﻿using System;
using osu.Framework.Graphics.UserInterface;

namespace timetable.Game.UI.Forms
{
    public class EditorDateBox : EditorCompositeBox<DateTime?>
    {
        private readonly EditorNumberBox year;
        private readonly EditorNumberBox month;
        private readonly EditorNumberBox day;
        private readonly EditorNumberBox hour;
        private readonly EditorNumberBox minute;
        private readonly EditorNumberBox second;

        public EditorDateBox()
            : base(DateTime.Now)
        {
            Disabled = true;

            Add(year = new FieldNumberBox(4)
            {
                Width = 50,
                PlaceholderText = "Year",
                TabbableContentContainer = Content
            });

            Add(month = new FieldNumberBox(2)
            {
                Width = 60,
                PlaceholderText = "Month",
                TabbableContentContainer = Content
            });

            Add(day = new FieldNumberBox(2)
            {
                Width = 40,
                PlaceholderText = "Day",
                TabbableContentContainer = Content
            });

            Add(hour = new FieldNumberBox(2)
            {
                Width = 50,
                PlaceholderText = "Hour",
                TabbableContentContainer = Content
            });

            Add(minute = new FieldNumberBox(2)
            {
                Width = 70,
                PlaceholderText = "Minute",
                TabbableContentContainer = Content
            });

            Add(second = new FieldNumberBox(2)
            {
                Width = 70,
                PlaceholderText = "Second",
                TabbableContentContainer = Content
            });

            year.OnCommit += updateDate;
            month.OnCommit += updateDate;
            day.OnCommit += updateDate;
            hour.OnCommit += updateDate;
            minute.OnCommit += updateDate;
            second.OnCommit += updateDate;
        }

        protected override void UpdateFields(DateTime? value)
        {
            if (!value.HasValue) return;

            var nonNullVal = value.Value;

            year.Current.Value = nonNullVal.Year;
            month.Current.Value = nonNullVal.Month;
            day.Current.Value = nonNullVal.Day;

            hour.Current.Value = nonNullVal.Hour;
            minute.Current.Value = nonNullVal.Minute;
            second.Current.Value = nonNullVal.Second;
        }

        protected override void ClearFields()
        {
            year.ClearValue();
            month.ClearValue();
            day.ClearValue();

            hour.ClearValue();
            minute.ClearValue();
            second.ClearValue();
        }

        private void updateDate(TextBox sender, bool newText)
        {
            if (!newText) return;

            try
            {
                if (year.Value.HasValue && month.Value.HasValue && day.Value.HasValue && hour.Value.HasValue && minute.Value.HasValue && second.Value.HasValue)
                    Current.Value = new DateTime(year.Value.Value, month.Value.Value, day.Value.Value, hour.Value.Value, minute.Value.Value, second.Value.Value);
            }
            catch (ArgumentOutOfRangeException)
            {
            }
        }
    }
}
