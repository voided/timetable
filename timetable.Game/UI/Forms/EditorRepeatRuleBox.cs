﻿using System;
using kyoseki.UI.Components.Input;
using osu.Framework.Bindables;
using osu.Framework.Graphics;
using osu.Framework.Graphics.Containers;
using osu.Framework.Graphics.UserInterface;
using osuTK;
using timetable.Game.Tasks;

namespace timetable.Game.UI.Forms
{
    public class EditorRepeatRuleBox : EditorCompositeBox<RepeatRule?>
    {
        private readonly EditorNumberBox year;
        private readonly EditorNumberBox month;
        private readonly EditorNumberBox day;

        private readonly KyosekiDropdown<RepeatType> typeDropdown;
        private readonly Container intervalContainer;
        private readonly EditorNumberBox interval;

        private readonly Container dayContainer;
        private readonly EditorDayBox days;

        public EditorRepeatRuleBox()
            : base(new RepeatRule(DateTime.Today, RepeatType.Interval, new TimeSpan(1, 0, 0, 0)))
        {
            DisplayClearCheckbox = false;

            AutoSizeAxes = Axes.Y;

            ContentFlow.Direction = FillDirection.Vertical;
            ContentFlow.Margin = FormConstants.MarginTall;

            Add(new LabelContainer("Start Date", FormConstants.FONT_SUBLABEL, true)
            {
                Child = new FillFlowContainer
                {
                    AutoSizeAxes = Axes.Both,
                    Direction = FillDirection.Horizontal,
                    Spacing = new Vector2(FormConstants.SPACING, 0),
                    Children = new Drawable[]
                    {
                        year = new FieldNumberBox(4)
                        {
                            Width = 50,
                            PlaceholderText = "Year",
                            TabbableContentContainer = Content
                        },
                        month = new FieldNumberBox(2)
                        {
                            Width = 60,
                            PlaceholderText = "Month",
                            TabbableContentContainer = Content
                        },
                        day = new FieldNumberBox(2)
                        {
                            Width = 40,
                            PlaceholderText = "Day",
                            TabbableContentContainer = Content
                        }
                    }
                }
            });

            Add(new FillFlowContainer
            {
                Name = $"{nameof(RepeatType)}-specific flow",
                RelativeSizeAxes = Axes.X,
                AutoSizeAxes = Axes.Y,
                Direction = FillDirection.Vertical,
                Children = new Drawable[]
                {
                    new LabelContainer("Rule Type", FormConstants.FONT_SUBLABEL, true)
                    {
                        Child = typeDropdown = new KyosekiDropdown<RepeatType>
                        {
                            Width = 200,
                            Items = new[] { RepeatType.Interval, RepeatType.OnDays }
                        }
                    },
                    intervalContainer = new LabelContainer("Interval (days)", FormConstants.FONT_SUBLABEL, true)
                    {
                        Child = interval = new FieldNumberBox(7)
                        {
                            Width = 120,
                            PlaceholderText = "Interval (days)",
                            TabbableContentContainer = Content
                        }
                    },
                    dayContainer = new LabelContainer("Repeat on", FormConstants.FONT_SUBLABEL)
                    {
                        RelativeSizeAxes = Axes.X,
                        Alpha = 0,
                        Child = days = new EditorDayBox { RelativeSizeAxes = Axes.X },
                        AlwaysPresent = true
                    }
                }
            });

            year.OnCommit += updateRule;
            month.OnCommit += updateRule;
            day.OnCommit += updateRule;
            interval.OnCommit += updateRule;
            days.Current.ValueChanged += e => updateRule(null, true);

            typeDropdown.Current.BindValueChanged(updateForm, true);
        }

        protected override void UpdateFields(RepeatRule? value)
        {
            if (!value.HasValue) return;

            var nonNullVal = value.Value;

            year.Current.Value = nonNullVal.StartDate.Year;
            month.Current.Value = nonNullVal.StartDate.Month;
            day.Current.Value = nonNullVal.StartDate.Day;

            typeDropdown.Current.Value = nonNullVal.Type;
            interval.Current.Value = nonNullVal.Interval?.Days;
            days.Current.Value = nonNullVal.Days ?? Days.None;
        }

        protected override void ClearFields()
        {
            year.ClearValue();
            month.ClearValue();
            day.ClearValue();

            interval.ClearValue();
            days.Current.Value = Days.None;
        }

        private void updateForm(ValueChangedEvent<RepeatType> e)
        {
            switch (e.NewValue)
            {
                case RepeatType.Interval:
                    intervalContainer.FadeIn(150);
                    dayContainer.FadeOut(150);
                    break;

                case RepeatType.OnDays:
                    dayContainer.FadeIn(150);
                    intervalContainer.FadeOut(150);
                    break;

                default: throw new Exception("tf?");
            }

            updateRule(null, true);
        }

        private void updateRule(TextBox sender, bool newText)
        {
            if (!newText) return;

            try
            {
                if (!(year.Value.HasValue && month.Value.HasValue && day.Value.HasValue)) return;

                var date = new DateTime(year.Value.Value, month.Value.Value, day.Value.Value);

                switch (typeDropdown.Current.Value)
                {
                    case RepeatType.Interval:
                        if (!interval.Value.HasValue) return;

                        Current.Value = new RepeatRule(date, RepeatType.Interval, new TimeSpan(interval.Value.Value, 0, 0, 0));
                        break;

                    case RepeatType.OnDays:
                        if (!days.Current.Value.HasValue) return;

                        Current.Value = new RepeatRule(date, RepeatType.OnDays, days: days.Current.Value);
                        break;

                    default: throw new Exception("tf?");
                }
            }
            catch (ArgumentOutOfRangeException)
            {
            }
        }
    }
}
