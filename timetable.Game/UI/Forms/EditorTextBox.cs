﻿using kyoseki.UI.Components;
using kyoseki.UI.Components.Theming;
using osu.Framework.Allocation;
using osu.Framework.Graphics;
using osu.Framework.Graphics.Colour;
using osu.Framework.Graphics.Shapes;
using osu.Framework.Graphics.Sprites;
using osu.Framework.Graphics.UserInterface;
using osuTK;
using timetable.Game.UI.Theming;

namespace timetable.Game.UI.Forms
{
    public class EditorTextBox : TextBox
    {
        private readonly Box background;

        public EditorTextBox()
        {
            Height = FormConstants.BOX_HEIGHT;

            Add(background = new Box
            {
                RelativeSizeAxes = Axes.Both,
                Depth = 1
            });
        }

        protected override void NotifyInputError() => background.FlashColour(Colour4.Red, 200);

        protected override Drawable GetDrawableCharacter(char c) => new ThemedText(CalculatedTextSize) { Text = c.ToString(), Font = FontUsage.Default.With(size: CalculatedTextSize) };

        protected override SpriteText CreatePlaceholder() => new ThemeablePlaceholderText
        {
            Anchor = Anchor.CentreLeft,
            Origin = Anchor.CentreLeft
        };

        protected override Caret CreateCaret() => new EditorCaret();

        private ColourInfo backgroundColourF;

        private ColourInfo backgroundColour
        {
            get => backgroundColourF;
            set
            {
                background.ClearTransforms();
                background.Colour = backgroundColourF = value;
            }
        }

        [ThemeLoader]
        protected void LoadTheme(TimetableTheme theme, bool fade)
        {
            this.TweenOrSet(nameof(backgroundColour), theme.Background3, fade); // TODO
        }

        [BackgroundDependencyLoader(true)]
        private void load(ThemeContainer themeContainer) => this.RegisterTo<TimetableTheme>(themeContainer);

        private class EditorCaret : Caret
        {
            private const int caret_width = 2;
            private const int caret_move_time = 60;

            public EditorCaret()
            {
                RelativeSizeAxes = Axes.Y;
                Size = new Vector2(1, 0.9f);

                Colour = Colour4.Transparent;
                Anchor = Anchor.CentreLeft;
                Origin = Anchor.CentreLeft;

                Masking = true;
                CornerRadius = 1;

                InternalChild = new Box
                {
                    RelativeSizeAxes = Axes.Both,
                    Colour = Colour4.White
                };
            }

            public override void Hide() => this.FadeOut(200);

            public override void DisplayAt(Vector2 position, float? selectionWidth)
            {
                if (selectionWidth != null)
                {
                    this.MoveTo(position, 60, Easing.Out);
                    this.ResizeWidthTo(selectionWidth.Value + caret_width / 2f, caret_move_time, Easing.Out);
                    this
                        .FadeTo(0.5f, 200, Easing.Out)
                        .FadeColour(Colour4.CornflowerBlue, 200, Easing.Out);
                }
                else
                {
                    this.MoveTo(new Vector2(position.X - caret_width / 2f, position.Y), 60, Easing.Out);
                    this.ResizeWidthTo(caret_width, caret_move_time, Easing.Out);
                    this
                        .FadeColour(Colour4.White, 200, Easing.Out)
                        .Loop(c => c.FadeTo(0.7f).FadeTo(0.4f, 500, Easing.InOutSine));
                }
            }
        }

        private class ThemeablePlaceholderText : SpriteText
        {
            [ThemeLoader]
            protected void LoadTheme(TimetableTheme theme, bool fade)
            {
                Font = theme.DefaultFont;
                this.TweenOrSet(nameof(Colour), theme.ForegroundColour, fade);
            }

            [BackgroundDependencyLoader(true)]
            private void load(ThemeContainer themeContainer) => this.RegisterTo<TimetableTheme>(themeContainer);
        }
    }
}
