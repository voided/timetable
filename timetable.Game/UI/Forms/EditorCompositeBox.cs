﻿using System;
using kyoseki.UI.Components.Input;
using osu.Framework.Bindables;
using osu.Framework.Graphics;
using osu.Framework.Graphics.Containers;
using osu.Framework.Graphics.UserInterface;
using osuTK;

namespace timetable.Game.UI.Forms
{
    /// <summary>
    /// A class providing somewhat consistent layout and features to an
    /// input box producing a certain datatype rather than just text.
    /// This component is auto sized on Y, and width should be determined manually through pixel Width value or RelativeSizeAxes.
    /// </summary>
    /// <typeparam name="T">The type to use for this input box. Must be nullable.</typeparam>
    public abstract class EditorCompositeBox<T> : Container, IHasCurrentValue<T>
    {
        private readonly BindableWithCurrent<T> current = new BindableWithCurrent<T>();

        public Bindable<T> Current
        {
            get => current.Current;
            set => current.Current = value;
        }

        private bool displayClearCheckbox = true;

        /// <summary>
        /// Whether to show the checkbox that sets this input's value to null.
        /// This does not automatically alter <see cref="Disabled"/>.
        /// If enabled, the user will not be able to set the value to null, but it can
        /// still be forced programmatically through <see cref="Disabled"/>.
        /// </summary>
        public bool DisplayClearCheckbox
        {
            get => displayClearCheckbox;
            set
            {
                if (displayClearCheckbox == value) return;

                displayClearCheckbox = value;

                if (value)
                    clearCheckbox.FadeIn(100);
                else
                    clearCheckbox.FadeOut(100);
            }
        }

        /// <summary>
        /// Whether this input's value should be set to null.
        /// </summary>
        public bool Disabled
        {
            get => clearCheckbox.Current.Value;
            set => clearCheckbox.Current.Value = value;
        }

        protected override Container<Drawable> Content { get; }

        protected readonly FillFlowContainer ContentFlow;

        private readonly Checkbox clearCheckbox;

        private T lastNonNullValue;

        // This should not be shared across all generic types.
        // ReSharper disable once StaticMemberInGenericType
        private static readonly bool valid_type;

        static EditorCompositeBox()
        {
            if (typeof(T).IsClass || Nullable.GetUnderlyingType(typeof(T)) != null)
                valid_type = true;
            else
                valid_type = false;
        }

        protected EditorCompositeBox(T defaultValue)
        {
            // Error should only be thrown in development
            if (!valid_type)
                throw new InvalidOperationException($"(development) {nameof(EditorCompositeBox<T>)} generic type should be nullable for consistent behavior.");

            if (defaultValue == null)
                throw new ArgumentException($"Default value for a {nameof(EditorCompositeBox<T>)} should not be null. If this behavior is intended, set {nameof(Disabled)} to true by default.");

            current.Value = lastNonNullValue = defaultValue;

            AutoSizeAxes = Axes.Y;
            AlwaysPresent = true;

            InternalChild = new FillFlowContainer
            {
                RelativeSizeAxes = Axes.X,
                AutoSizeAxes = Axes.Y,
                Direction = FillDirection.Horizontal,
                Spacing = new Vector2(FormConstants.SPACING, 0),
                Children = new Drawable[]
                {
                    Content = ContentFlow = new FillFlowContainer
                    {
                        Name = "custom form content",
                        AutoSizeAxes = Axes.Both,
                        Direction = FillDirection.Horizontal,
                        Spacing = new Vector2(FormConstants.SPACING)
                    },
                    clearCheckbox = new KyosekiCheckbox
                    {
                        LabelText = "Clear Value",
                        Anchor = Anchor.CentreLeft,
                        Origin = Anchor.CentreLeft
                    }
                }
            };
        }

        private bool updating;

        protected override void LoadComplete()
        {
            base.LoadComplete();

            current.BindValueChanged(e =>
            {
                if (updating) return;

                var val = e.NewValue;

                if (val != null)
                {
                    if (clearCheckbox.Current.Value)
                    {
                        lastNonNullValue = val;
                        current.Value = default;
                        return;
                    }

                    // schedule after children to ensure that fields get updated.
                    // this *will* cause unusual behavior if the forms are not loaded at all times.
                    ScheduleAfterChildren(() =>
                    {
                        updating = true;
                        UpdateFields(val);
                        updating = false;
                    });

                    Disabled = false;
                }
                else
                {
                    ScheduleAfterChildren(ClearFields);

                    Disabled = true;
                }
            }, true);

            clearCheckbox.Current.BindValueChanged(e =>
            {
                if (e.NewValue)
                {
                    if (current.Value != null)
                        lastNonNullValue = current.Value;

                    current.Value = default;
                }
                else
                {
                    current.Value = lastNonNullValue;
                }
            }, true);
        }

        /// <summary>
        /// Called whenever the fields' values should be updated.
        /// Not for the purpose of updating the value of <see cref="Current"/>.
        /// </summary>
        /// <param name="value">The value the fields should reflect.</param>
        protected abstract void UpdateFields(T value);

        /// <summary>
        /// Called whenever this input's value is set to null.
        /// Should clear the value of all fields.
        /// </summary>
        protected abstract void ClearFields();
    }
}
