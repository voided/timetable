﻿using osu.Framework.Graphics;

namespace timetable.Game.UI.Forms
{
    public static class FormConstants
    {
        public const int BOX_HEIGHT = 20;
        public const int SPACING = 5;
        public const int FONT_SUBLABEL = 12;
        public static MarginPadding MarginTall = new MarginPadding { Left = 10 };
    }
}
