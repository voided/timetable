﻿using System;
using osu.Framework.Bindables;
using osu.Framework.Graphics.UserInterface;

namespace timetable.Game.UI.Forms
{
    // TODO: Fix OverflowException
    public class EditorNumberBox : EditorTextBox, IHasCurrentValue<int?>
    {
        private readonly BindableWithCurrent<int?> current = new BindableWithCurrent<int?>();

        public new Bindable<int?> Current
        {
            get => current;
            set => current.Current = value;
        }

        public int? Value => current.Value;

        private readonly int limit;

        public EditorNumberBox(int limit = int.MaxValue, int padding = 2)
        {
            this.limit = limit;

            base.Current.ValueChanged += e =>
            {
                current.Value = string.IsNullOrEmpty(e.NewValue) ? null as int? : int.Parse(e.NewValue);
            };

            Current.ValueChanged += e =>
            {
                base.Current.Value = e.NewValue.HasValue ? e.NewValue.Value.ToString("D" + Math.Min(padding, e.NewValue.Value.ToString().Length)) : string.Empty;
            };
        }

        public void ClearValue()
        {
            if (Current.Disabled) return;

            Current.Value = null;
        }

        protected override bool CanAddCharacter(char character) => char.IsNumber(character) && Text.Length < limit;
    }
}
