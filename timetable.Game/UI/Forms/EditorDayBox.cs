﻿using kyoseki.UI.Components;
using kyoseki.UI.Components.Theming;
using osu.Framework.Allocation;
using osu.Framework.Bindables;
using osu.Framework.Extensions.IEnumerableExtensions;
using osu.Framework.Graphics;
using osu.Framework.Graphics.Colour;
using osu.Framework.Graphics.Containers;
using osu.Framework.Graphics.Shapes;
using osu.Framework.Graphics.UserInterface;
using osuTK;
using timetable.Game.Tasks;
using timetable.Game.UI.Theming;

namespace timetable.Game.UI.Forms
{
    public class EditorDayBox : EditorCompositeBox<Days?>
    {
        private readonly DayCheckbox[] checkboxes;

        public EditorDayBox()
            : base(Days.None)
        {
            DayCheckbox monday;
            DayCheckbox tuesday;
            DayCheckbox wednesday;
            DayCheckbox thursday;
            DayCheckbox friday;
            DayCheckbox saturday;
            DayCheckbox sunday;

            DisplayClearCheckbox = false;

            Add(new FillFlowContainer
            {
                AutoSizeAxes = Axes.Both,
                Spacing = new Vector2(2, 0),
                Direction = FillDirection.Horizontal,
                Children = new[]
                {
                    monday = new DayCheckbox(Days.Monday),
                    tuesday = new DayCheckbox(Days.Tuesday),
                    wednesday = new DayCheckbox(Days.Wednesday),
                    thursday = new DayCheckbox(Days.Thursday),
                    friday = new DayCheckbox(Days.Friday),
                    saturday = new DayCheckbox(Days.Saturday),
                    sunday = new DayCheckbox(Days.Sunday)
                }
            });

            checkboxes = new[] { monday, tuesday, wednesday, thursday, friday, saturday, sunday };

            checkboxes.ForEach(c => c.Current.BindValueChanged(updateDays, true));
        }

        protected override void UpdateFields(Days? value)
        {
            if (!value.HasValue) return;

            checkboxes.ForEach(c => c.Current.Value = value.Value.HasFlag(c.Day));
        }

        protected override void ClearFields() => checkboxes.ForEach(c => c.Current.Value = false);

        private void updateDays(ValueChangedEvent<bool> e)
        {
            var days = Days.None;

            checkboxes.ForEach(c => days |= c.Current.Value ? c.Day : Days.None);

            Current.Value = days;
        }

        private class DayCheckbox : Checkbox
        {
            public readonly Days Day;

            private readonly Circle background;

            public DayCheckbox(Days day)
            {
                Day = day;
                AutoSizeAxes = Axes.Both;
                Anchor = Anchor.Centre;
                Origin = Anchor.Centre;

                Children = new Drawable[]
                {
                    background = new Circle { Size = new Vector2(20) },
                    new ThemedText(12)
                    {
                        Anchor = Anchor.Centre,
                        Origin = Anchor.Centre,
                        Text = day.ToWeekString()
                    }
                };

                Current.BindValueChanged(e => background.FadeColour(e.NewValue ? backgroundSelected : backgroundUnselected, 100), true);
            }

            private ColourInfo backgroundUnselectedF;

            private ColourInfo backgroundUnselected
            {
                get => backgroundUnselectedF;
                set
                {
                    backgroundUnselectedF = value;

                    if (!Current.Value)
                        background.Colour = value;
                }
            }

            private ColourInfo backgroundSelectedF;

            private ColourInfo backgroundSelected
            {
                get => backgroundSelectedF;
                set
                {
                    backgroundSelectedF = value;

                    if (Current.Value)
                        background.Colour = value;
                }
            }

            [ThemeLoader]
            protected void LoadTheme(TimetableTheme theme, bool fade)
            {
                this.TweenOrSet(nameof(backgroundUnselected), theme.ButtonBackground, fade);
                this.TweenOrSet(nameof(backgroundSelected), theme.AccentColour, fade);
            }

            [BackgroundDependencyLoader(true)]
            private void load(ThemeContainer themeContainer) => this.RegisterTo<TimetableTheme>(themeContainer);
        }
    }
}
