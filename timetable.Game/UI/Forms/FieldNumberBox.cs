﻿namespace timetable.Game.UI.Forms
{
    public class FieldNumberBox : EditorNumberBox
    {
        public FieldNumberBox(int limit = int.MaxValue)
            : base(limit)
        {
            CommitOnFocusLost = true;
        }
    }
}
