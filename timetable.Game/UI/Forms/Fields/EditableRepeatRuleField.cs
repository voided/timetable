﻿using timetable.Game.Tasks;

namespace timetable.Game.UI.Forms.Fields
{
    public class EditableRepeatRuleField : EditableField<EditorRepeatRuleBox, RepeatRule?>
    {
        public EditableRepeatRuleField(string labelText, EditorRepeatRuleBox form)
            : base(labelText, form)
        {
        }

        protected override string GetValueString() =>
            Form.Current.Value.HasValue
                ? Form.Current.Value.ToString()
                : "<no value>";
    }
}
