﻿using kyoseki.UI.Components;
using kyoseki.UI.Components.Theming;
using osu.Framework.Allocation;
using osu.Framework.Bindables;
using osu.Framework.Graphics;
using osu.Framework.Graphics.Containers;
using osu.Framework.Graphics.Shapes;
using osu.Framework.Graphics.Sprites;
using osu.Framework.Graphics.UserInterface;
using timetable.Game.UI.Theming;

namespace timetable.Game.UI.Forms.Fields
{
    public abstract class EditableField<TDrawable, TValue> : CompositeDrawable, IEditableField
        where TDrawable : CompositeDrawable, IHasCurrentValue<TValue>
    {
        public Bindable<TValue> Current => Form.Current;

        private bool editable;

        public bool Editable
        {
            get => editable;
            set
            {
                if (value == editable)
                    return;

                editable = value;

                if (editable)
                {
                    Form.FadeIn(200, Easing.OutQuad);
                    valueContainer.FadeOut(200, Easing.InQuad);
                }
                else
                {
                    Form.FadeOut(200, Easing.InQuad);
                    valueContainer.FadeIn(200, Easing.OutQuad);
                }
            }
        }

        public readonly TDrawable Form;

        private readonly Container valueContainer;
        private readonly Box background;

        protected EditableField(string labelText, TDrawable form)
        {
            Form = form;

            SpriteText valueText;

            AutoSizeAxes = Axes.Y;

            InternalChildren = new Drawable[]
            {
                new LabelContainer(labelText)
                {
                    RelativeSizeAxes = Axes.X,
                    AutoSizeAxes = Axes.Y,
                    Children = new Drawable[]
                    {
                        valueContainer = new Container
                        {
                            RelativeSizeAxes = Axes.X,
                            Height = FormConstants.BOX_HEIGHT,
                            Children = new Drawable[]
                            {
                                background = new Box
                                {
                                    RelativeSizeAxes = Axes.Both
                                },
                                valueText = new ThemedText(FormConstants.BOX_HEIGHT)
                                {
                                    Text = GetValueString(),
                                    Anchor = Anchor.CentreLeft,
                                    Origin = Anchor.CentreLeft,
                                    Truncate = true,
                                    Padding = new MarginPadding { Left = 5 }
                                }
                            }
                        },
                        form.With(c =>
                        {
                            c.RelativeSizeAxes = Axes.X;
                            c.Anchor = Anchor.TopLeft;
                            c.Origin = Anchor.TopLeft;
                            c.Alpha = 0;
                        })
                    }
                }
            };

            form.Current.ValueChanged += e => valueText.Text = GetValueString();
        }

        protected abstract string GetValueString();

        [ThemeLoader]
        protected void LoadTheme(TimetableTheme theme, bool fade)
        {
            background.TweenOrSet(nameof(background.Colour), theme.Background3, fade); // TODO
        }

        [BackgroundDependencyLoader(true)]
        private void load(ThemeContainer themeContainer) => this.RegisterTo<TimetableTheme>(themeContainer);
    }

    public interface IEditableField
    {
        bool Editable { get; set; }
    }
}
