﻿namespace timetable.Game.UI.Forms.Fields
{
    public class EditableNumberField : EditableField<EditorNumberBox, int?>
    {
        public EditableNumberField(string labelText, EditorNumberBox form)
            : base(labelText, form)
        {
        }

        protected override string GetValueString() => Form.Value.HasValue ? Form.Value.Value.ToString() : "<no value>";
    }
}
