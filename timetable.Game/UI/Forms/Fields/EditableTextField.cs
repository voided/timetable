﻿namespace timetable.Game.UI.Forms.Fields
{
    public class EditableTextField : EditableField<EditorTextBox, string>
    {
        public EditableTextField(string labelText, EditorTextBox form)
            : base(labelText, form)
        {
            form.PlaceholderText = labelText;
        }

        protected override string GetValueString() => Form.Current.Value;
    }
}
