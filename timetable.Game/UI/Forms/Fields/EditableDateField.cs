﻿using System;

namespace timetable.Game.UI.Forms.Fields
{
    public class EditableDateField : EditableField<EditorDateBox, DateTime?>
    {
        private const string full_date_format = @"yyyy/MM/dd (dddd) HH:mm:ss %K";

        public EditableDateField(string labelText, EditorDateBox form)
            : base(labelText, form)
        {
        }

        protected override string GetValueString() => Form.Current.Value.HasValue ? Form.Current.Value.Value.ToString(full_date_format) : "<no value>";
    }
}
