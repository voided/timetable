﻿using kyoseki.UI.Components;
using osu.Framework.Graphics;
using osu.Framework.Graphics.Containers;

namespace timetable.Game.UI.Forms
{
    public class LabelContainer : Container
    {
        protected override Container<Drawable> Content { get; }

        public LabelContainer(string text, int size = 20, bool inline = false)
        {
            AutoSizeAxes = inline ? Axes.Both : Axes.Y;

            InternalChildren = new Drawable[]
            {
                new ThemedText(size)
                {
                    RelativeSizeAxes = Axes.X,
                    Text = text,
                    Truncate = true
                },
                Content = new Container
                {
                    AutoSizeAxes = inline ? Axes.Both : Axes.Y,
                    RelativeSizeAxes = inline ? Axes.None : Axes.X,
                    Padding = new MarginPadding { Top = size }
                }
            };
        }
    }
}
