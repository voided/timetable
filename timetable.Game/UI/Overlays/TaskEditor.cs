﻿using System.Diagnostics;
using kyoseki.UI.Components.Buttons;
using kyoseki.UI.Components.Theming;
using osu.Framework.Allocation;
using osu.Framework.Graphics;
using osu.Framework.Graphics.Containers;
using osu.Framework.Graphics.Shapes;
using osu.Framework.Graphics.Sprites;
using osu.Framework.Input.Bindings;
using osu.Framework.Input.Events;
using osuTK;
using timetable.Game.Input.Bindings;
using timetable.Game.Tasks.Stores;
using timetable.Game.Tasks.Types;
using timetable.Game.UI.Forms;
using timetable.Game.UI.Forms.Fields;
using timetable.Game.UI.Theming;

namespace timetable.Game.UI.Overlays
{
    public class TaskEditor : FocusedOverlayContainer, IKeyBindingHandler<GlobalAction>
    {
        private const int transition_duration = 200;

        private Task task = new GeneralTask();

        [Resolved(canBeNull: true)]
        private TaskStore store { get; set; }

        public Task Task
        {
            get => task;
            set
            {
                task = value;

                updateTask();
            }
        }

        private readonly Box background;

        private readonly TaskEditorButton editButton;

        private readonly EditableTextField nameField;
        private readonly EditableTextField groupField;
        private readonly EditableTextField descField;
        private readonly EditableNumberField priorityField;
        private readonly EditableDateField dateField;

        private readonly EditableRepeatRuleField repeatField;

        public TaskEditor()
        {
            RelativePositionAxes = Axes.Y;
            Size = new Vector2(1000, 500);
            Anchor = Anchor.Centre;
            Origin = Anchor.Centre;
            CornerRadius = 50;
            Masking = true;
            AlwaysPresent = true;

            InternalChildren = new Drawable[]
            {
                background = new Box
                {
                    RelativeSizeAxes = Axes.Both
                },
                new FillFlowContainer
                {
                    RelativeSizeAxes = Axes.Both,
                    Direction = FillDirection.Vertical,
                    Padding = new MarginPadding(50),
                    Spacing = new Vector2(0, 5),
                    Children = new Drawable[]
                    {
                        nameField = new EditableTextField("Name", new EditorTextBox()) { Width = 700 },
                        groupField = new EditableTextField("Group", new EditorTextBox()) { Width = 700 },
                        descField = new EditableTextField("Description", new EditorTextBox()) { RelativeSizeAxes = Axes.X },
                        priorityField = new EditableNumberField("Priority", new EditorNumberBox(3)) { Width = 100 },
                        new Container
                        {
                            AutoSizeAxes = Axes.Y,
                            RelativeSizeAxes = Axes.X,
                            Children = new Drawable[]
                            {
                                dateField = new EditableDateField("Due Date", new EditorDateBox()) { RelativeSizeAxes = Axes.X, AlwaysPresent = true },
                                repeatField = new EditableRepeatRuleField("Repeat Rule", new EditorRepeatRuleBox()) { RelativeSizeAxes = Axes.X, AlwaysPresent = true }
                            }
                        }
                    }
                },
                new FillFlowContainer
                {
                    AutoSizeAxes = Axes.Both,
                    Anchor = Anchor.TopRight,
                    Origin = Anchor.TopRight,
                    Direction = FillDirection.Horizontal,
                    Margin = new MarginPadding(30),
                    Spacing = new Vector2(5, 0),
                    Children = new Drawable[]
                    {
                        editButton = new TaskEditorButton
                        {
                            Icon = FontAwesome.Solid.Edit,
                            IconSize = new Vector2(0.55f),
                            Action = () => toggleEditing()
                        },
                        new TaskEditorButton
                        {
                            Icon = FontAwesome.Solid.Times,
                            IconSize = new Vector2(0.45f),
                            Action = Hide
                        }
                    }
                }
            };

            ScheduleAfterChildren(() => AlwaysPresent = false);
            updateTask();
        }

        protected override void PopIn()
        {
            if (editing)
                toggleEditing(false);

            this.FadeIn(transition_duration, Easing.OutQuad);
            this.MoveToY(0, transition_duration, Easing.OutQuad);

            base.PopIn();
        }

        protected override void PopOut()
        {
            this.FadeOut(transition_duration, Easing.InQuad);
            this.MoveToY(0.5f, transition_duration, Easing.InQuad);

            base.PopOut();
        }

        protected override void LoadComplete()
        {
            base.LoadComplete();

            if (store != null)
                store.ItemDeleted += taskDeleted;
        }

        private void updateTask()
        {
            nameField.Current.Value = task.Name;
            groupField.Current.Value = task.Group;
            descField.Current.Value = task.Description;
            priorityField.Current.Value = task.Priority;

            switch (task)
            {
                case GeneralTask gt:
                    dateField.Current.Value = gt.Due;
                    dateField.Form.Disabled = gt.Due == null;

                    setRepeat(false);
                    break;

                case IHasRepeatRule hrr:
                    repeatField.Current.Value = hrr.RepeatRule;
                    setRepeat(true);
                    break;
            }
        }

        private bool editing;

        private void toggleEditing(bool save = true)
        {
            editing = !editing;

            editButton.Icon = editing ? FontAwesome.Solid.Save : FontAwesome.Solid.Edit;

            if (!editing && save)
            {
                store?.Modify(task, t =>
                {
                    t.Name = nameField.Current.Value;
                    t.Group = groupField.Current.Value;
                    t.Description = descField.Current.Value;

                    priorityField.Current.Value ??= 0;
                    t.Priority = priorityField.Current.Value.Value;

                    switch (task)
                    {
                        case GeneralTask gt:
                            gt.Due = dateField.Current.Value;
                            break;

                        case IHasRepeatRule hrr:
                            Trace.Assert(repeatField.Current.Value.HasValue);
                            hrr.RepeatRule = repeatField.Current.Value.Value;
                            break;
                    }
                });
            }

            nameField.Editable = editing;
            groupField.Editable = editing;
            descField.Editable = editing;
            priorityField.Editable = editing;
            dateField.Editable = editing;
            repeatField.Editable = editing;
        }

        private void setRepeat(bool repeat)
        {
            if (repeat)
            {
                repeatField.Form.Disabled = false;

                dateField.FadeOut(150).OnComplete(d =>
                {
                    dateField.Form.Disabled = true;
                });
                repeatField.FadeIn(150);
            }
            else
            {
                repeatField.FadeOut(150).OnComplete(d =>
                {
                    repeatField.Form.Disabled = true;
                });
                dateField.FadeIn(150);
            }
        }

        private void taskDeleted(Task t)
        {
            if (t.Id != Task.Id) return;

            Task = new GeneralTask();
            Hide();
        }

        protected override void Dispose(bool isDisposing)
        {
            if (store != null)
                store.ItemDeleted -= taskDeleted;
        }

        [ThemeLoader]
        protected void LoadTheme(TimetableTheme theme, bool fade)
        {
            background.TweenOrSet(nameof(background.Colour), theme.Background4, fade);
        }

        [BackgroundDependencyLoader(true)]
        private void load(ThemeContainer themeContainer) => this.RegisterTo<TimetableTheme>(themeContainer);

        public bool OnPressed(KeyBindingPressEvent<GlobalAction> e)
        {
            if (e.Action != GlobalAction.Back) return false;

            Hide();
            return true;
        }

        public void OnReleased(KeyBindingReleaseEvent<GlobalAction> e)
        {
        }

        private class TaskEditorButton : IconButton
        {
            public TaskEditorButton()
            {
                Size = new Vector2(25);
                Masking = true;
                CornerRadius = 12.5f;
            }
        }
    }
}
