﻿using osu.Framework.Graphics;
using osuTK;

namespace timetable.Game.UI
{
    public interface IDropContainer<T> : IDrawable
    {
        bool CanDrop(DragDropItem<T> item);

        /// <summary>
        /// Should return the target container (in cases where the target and this container are the same, it is acceptable to return this),
        /// and the position (local to the container) at which the model should be inserted.
        /// </summary>
        (Drawable, Vector2) GetInsertPosition(T model);

        /// <summary>
        /// Initiates a drag from this container.
        /// May handle fade out of the item, etc.
        /// </summary>
        /// <param name="item">The item that has been dragged. May be stored for <see cref="OnDragFinalize"/></param>
        void OnDrag(DragDropItem<T> item);

        /// <summary>
        /// Called when the drag initialized by <see cref="OnDrag"/> is finalized.
        /// Depending on implementation, this method may handle the item being removed from the container.
        /// </summary>
        /// <param name="item">The item that has been dragged out of this container.</param>
        void OnDragFinalize(DragDropItem<T> item);

        /// <summary>
        /// Called after <see cref="GetInsertPosition"/> when a drop is finalized.
        /// This should handle any fades, etc. when an item is dropped.
        /// It is also called when an item returns to this drawable (i.e. the drop was rejected),
        /// so any properties created during <see cref="GetInsertPosition"/> may also be null.
        /// </summary>
        /// <returns>The item created during the drop. Can be null.</returns>
        DragDropItem<T> OnDrop(T model);

        /// <summary>
        /// Should indicate whether the container contains the given <paramref name="item"/>.
        /// Ensures proper animations when drag is cancelled.
        /// </summary>
        bool Contains(DragDropItem<T> item);
    }
}
