﻿using System;
using System.Collections.Generic;
using System.Linq;
using osu.Framework.Allocation;
using osu.Framework.Graphics;
using osu.Framework.Graphics.Containers;
using osu.Framework.Graphics.Cursor;
using osu.Framework.Graphics.Transforms;
using osu.Framework.Input.Events;
using osu.Framework.Testing;
using osuTK;

namespace timetable.Game.UI
{
    /// <summary>
    /// A container providing drag and drop abilities to its <see cref="IDropContainer{T}"/> children.
    /// </summary>
    /// <remarks>All <typeparamref name="T"/> should come from the same source and be exactly equal to one another for consistent behavior.</remarks>
    [Cached]
    public abstract class DragDropContainer<T> : CursorEffectContainer<DragDropContainer<T>, DragDropItem<T>>
        where T : class
    {
        /// <summary>
        /// The state stored for every drop occurring in this container right now.
        /// </summary>
        private readonly Dictionary<T, DragContext> context = new Dictionary<T, DragContext>();

        /// <summary>
        /// The item that is currently being dragged with the mouse.
        /// </summary>
        private T draggedItem;

        private Container cursorContainer;

        /// <summary>
        /// This event is fired with the cursor each time it appears.
        /// </summary>
        public event Action<DragDropItem<T>> OnCursorActive;

        /// <summary>
        /// This event is fired with the cursor and the original item each time it fades out.
        /// </summary>
        public event Action<DragDropItem<T>, DragDropItem<T>> OnCursorInactive;

        [BackgroundDependencyLoader]
        private void load()
        {
            AddInternal(cursorContainer = new Container
            {
                RelativeSizeAxes = Axes.Both,
                Depth = -int.MaxValue
            });
        }

        /// <summary>
        /// Programmatically drags an item from <paramref name="item"/> to
        /// <paramref name="dst"/>
        /// </summary>
        /// <returns>Whether the drag & drop were successful</returns>
        public bool DragTo(DragDropItem<T> item, IDropContainer<T> dst)
        {
            var res1 = beginDrag(item);
            return res1 && drop(item.Model, dst);
        }

        /// <summary>
        /// Creates a copy of the drag item to use as a cursor.
        /// </summary>
        protected abstract DragDropItem<T> CreateCursor();

        protected override bool OnDragStart(DragStartEvent e)
        {
            if (draggedItem != null) return false;

            var child = FindTargets().FirstOrDefault();
            if (child == null) return false;

            if (!beginDrag(child)) return false;

            draggedItem = child.Model;

            return true;
        }

        protected override void OnDrag(DragEvent e)
        {
            var ctx = context[draggedItem];
            ctx.CursorDrawable.Position += e.Delta;

            base.OnDrag(e);
        }

        protected override void OnDragEnd(DragEndEvent e)
        {
            if (draggedItem == null) return;

            var ctx = context[draggedItem];

            // TODO: Don't use ChildrenOfType
            foreach (var dst in this.ChildrenOfType<IDropContainer<T>>())
            {
                if (dst == ctx.DragItem.Parent || dst.Contains(ctx.DragItem) || !dst.ScreenSpaceDrawQuad.Contains(e.ScreenSpaceMousePosition)) continue;

                drop(draggedItem, dst);

                draggedItem = null;

                // OK to always return as drop will automatically cancel
                // if it cannot be performed
                return;
            }

            // Handle if no container or the originating container is the destination
            cancelDrop(draggedItem);

            draggedItem = null;

            // base method should not need to be called since all possible situations should be handled by now
            // a drag would not have initiated unless a target was found
        }

        /// <summary>
        /// Converts the given <paramref name="pos"/> to the local space,
        /// adjusting for Anchor (of the <paramref name="target"/>) such that the positions are visually the same
        /// between the spaces of the <paramref name="parent"/> and this container.
        /// </summary>
        private Vector2 toLocalSpace(IDrawable parent, Drawable target, Vector2 pos)
        {
            var converted = parent.ToSpaceOfOtherDrawable(pos, this);
            var offset = target.RelativeAnchorPosition * (DrawSize - parent.DrawSize);

            return converted - offset;
        }

        /// <summary>
        /// Prepares the provided item to be dragged.
        /// </summary>
        /// <returns>Whether the drag was successful</returns>
        private bool beginDrag(DragDropItem<T> item)
        {
            if (item.DropContainer == null || context.ContainsKey(item.Model)) return false;

            var cursor = CreateCursor();
            cursor.Model = item.Model;

            var dragStartPos = cursor.Position = toLocalSpace(item.Parent, item, item.Position);

            cursorContainer.Add(cursor);
            item.OnDrag(cursor);
            item.DropContainer.OnDrag(item);

            context.Add(item.Model, new DragContext(cursor, item, dragStartPos));
            OnCursorActive?.Invoke(cursor);

            return true;
        }

        /// <summary>
        /// Drops the currently held drawable into the provided container
        /// </summary>
        /// <returns>Whether the drop was successful</returns>
        private bool drop(T dragged, IDropContainer<T> dst)
        {
            var ctx = context[dragged];

            if (!dst.CanDrop(ctx.CursorDrawable))
            {
                cancelDrop(dragged);
                return false;
            }

            // Get position where cursor should animate to
            var (realDst, pos) = dst.GetInsertPosition(ctx.CursorDrawable.Model);
            var targetPos = toLocalSpace(realDst, ctx.CursorDrawable, pos);

            ctx.DragItem.DropContainer.OnDragFinalize(ctx.DragItem);

            TransformSequence<DragDropItem<T>> transformSequence;

            if (dst is IHasCustomDropAnimation<T> custom)
                transformSequence = custom.Animate(ctx.CursorDrawable, targetPos);
            else
                transformSequence = ctx.CursorDrawable.MoveTo(targetPos, 300, Easing.OutQuint);

            transformSequence.OnComplete(_ =>
            {
                var item = dst.OnDrop(dragged);
                ctx.CursorDrawable.OnDrop(item, dst);

                ctx.CursorDrawable.Delay(100).FadeOut().OnComplete(_ =>
                {
                    ctx.CursorDrawable.Expire();
                    context.Remove(dragged);

                    OnCursorInactive?.Invoke(ctx.CursorDrawable, item);
                });
            });

            return true;
        }

        /// <summary>
        /// Cancels the current drag/drop operation
        /// </summary>
        private void cancelDrop(T dragged)
        {
            var ctx = context[dragged];

            ctx.CursorDrawable.MoveTo(ctx.DragStartPos, 300, Easing.OutQuint).OnComplete(_ =>
            {
                ctx.DragItem.FadeIn();

                ctx.DragItem.DropContainer.OnDrop(dragged);
                ctx.CursorDrawable.OnDrop(ctx.DragItem, ctx.DragItem.DropContainer);

                ctx.CursorDrawable.Expire();
                context.Remove(dragged);

                OnCursorInactive?.Invoke(ctx.CursorDrawable, ctx.DragItem);
            });
        }

        private struct DragContext
        {
            /// <summary>
            /// Drawable rendered as the mouse is dragging
            /// </summary>
            public DragDropItem<T> CursorDrawable { get; }

            /// <summary>
            /// The copy of the item being dragged, in its original container.
            /// </summary>
            public DragDropItem<T> DragItem { get; }

            public Vector2 DragStartPos { get; }

            public DragContext(DragDropItem<T> cursorDrawable, DragDropItem<T> dragItem, Vector2 dragStartPos)
            {
                CursorDrawable = cursorDrawable;
                DragItem = dragItem;
                DragStartPos = dragStartPos;
            }
        }
    }
}
