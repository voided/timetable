﻿using System;
using System.Collections.Generic;
using System.Linq;
using osu.Framework.Graphics;
using osu.Framework.Graphics.Containers;
using osuTK;

namespace timetable.Game.UI
{
    public class FillFlowDropContainer<TItem, TModel> : FillFlowContainer<TItem>, IDropContainer<TModel>
        where TItem : DragDropItem<TModel>, new()
    {
        public override IEnumerable<Drawable> FlowingChildren => Children.OrderBy(c => GetInsertIndex(c.Model));

        protected virtual int GetInsertIndex(TModel model)
        {
            try
            {
                var list = Children.Select(c => c.Model).ToList();
                list.Sort();

                var result = list.BinarySearch(model);

                return result < 0 ? ~result : result;
            }
            catch (Exception e)
                when (e is ArgumentException || e is InvalidOperationException)
            {
                return Children.Count;
            }
        }

        public void Add(TModel model) =>
            Add(new TItem
            {
                Model = model
            });

        public FillFlowDropContainer()
        {
            LayoutEasing = Easing.OutExpo;
            LayoutDuration = 200;
        }

        public bool CanDrop(DragDropItem<TModel> item) => true;

        private readonly Dictionary<TModel, TItem> placeholders = new Dictionary<TModel, TItem>();

        public (Drawable, Vector2) GetInsertPosition(TModel model)
        {
            TItem placeholder;

            Add(placeholder = new TItem
            {
                Model = model,
                Alpha = 0
            });

            placeholders.Add(model, placeholder);

            // Compute the position of the placeholder in this container,
            // without requiring any layout updates to occur
            return (this, ComputeLayoutPositions().ElementAt(FlowingChildren.ToList().IndexOf(placeholder)));
        }

        public void OnDrag(DragDropItem<TModel> item) => item.FadeOut();

        public void OnDragFinalize(DragDropItem<TModel> item) => item.Expire();

        public DragDropItem<TModel> OnDrop(TModel model)
        {
            placeholders.TryGetValue(model, out TItem placeholder);

            if (placeholder != null)
            {
                placeholders.Remove(model);
                placeholder.FadeIn();
            }

            return placeholder;
        }

        public bool Contains(DragDropItem<TModel> item) => Children.Contains(item);
    }
}
