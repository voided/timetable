﻿using kyoseki.UI.Components.Theming;
using osu.Framework.Graphics;
using osu.Framework.Graphics.Sprites;

namespace timetable.Game.UI.Theming
{
    /// <summary>
    /// Timetable's hue-based theme.
    /// Takes many inspirations from osu!'s color system.
    /// </summary>
    public class TimetableTheme : UITheme
    {
        public override string Name => "timetable";

        private const float hue = 209 / 360f;

        public override FontUsage DefaultFont => new FontUsage("Manrope");

        public virtual Colour4 Background1 => colour(0.5f, 0.4f);
        public virtual Colour4 Background2 => colour(0.5f, 0.3f);
        public virtual Colour4 Background3 => colour(0.5f, 0.2f);
        public virtual Colour4 Background4 => colour(0.5f, 0.15f);
        public virtual Colour4 Background5 => colour(0.5f, 0.1f);
        public virtual Colour4 Background6 => colour(0.5f, 0.05f);

        public override Colour4 BackgroundColour => Background5;

        public virtual Colour4 Content1 => colour(0.3f, 0.95f);
        public virtual Colour4 Content2 => colour(0.6f, 0.8f);
        public virtual Colour4 Content3 => colour(0.6f, 0.7f);

        public override Colour4 ForegroundColour => Content1;
        public override Colour4 ForegroundSelected => Colour4.White;

        // Used for links/buttons
        public virtual Colour4 Link1 => colour(0.2f, 0.6f);
        public virtual Colour4 Link2 => colour(0.2f, 0.5f);
        public virtual Colour4 Link3 => colour(0.2f, 0.4f);
        public virtual Colour4 Link4 => colour(0.2f, 0.3f);

        public virtual Colour4 Shadow => Colour4.Black.Opacity(0.5f);

        public override Colour4 ButtonBackground => Link3;
        public override Colour4 ButtonSelected => Link2;

        public override Colour4 AccentColour => colour(0.75f, 0.6f);

        public override Colour4 TextBoxUnfocused => Link3;
        public override Colour4 TextBoxFocused => Link2;
        public override Colour4 TextBoxReadOnly => Link1;
        public override Colour4 TextSelected => TextBoxReadOnly.Lighten(0.4f);

        private Colour4 colour(float saturation, float lightness) => Colour4.FromHSL(hue, saturation, lightness);
    }
}
