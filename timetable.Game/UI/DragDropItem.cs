﻿using osu.Framework.Graphics.Containers;

namespace timetable.Game.UI
{
    public abstract class DragDropItem<T> : CompositeDrawable
    {
        public IDropContainer<T> DropContainer
        {
            get
            {
                if (Parent is IDropContainer<T> d)
                    return d;

                return null;
            }
        }

        // Always true to work with CursorEffectContainer
        public override bool HandlePositionalInput => true;

        public abstract T Model { get; set; }

        /// <summary>
        /// This method gets called when an item is dragged.
        /// At this point, "this" refers to the originally dragged item,
        /// and <paramref name="cursor"/> refers to the cursor drawable.
        /// </summary>
        public virtual void OnDrag(DragDropItem<T> cursor)
        {
        }

        /// <summary>
        /// This method gets called when an item is dropped.
        /// At this point, "this" refers to the cursor drawable,
        /// and <paramref name="target" /> refers to the dropped drawable (if any).
        /// </summary>
        public virtual void OnDrop(DragDropItem<T> target, IDropContainer<T> dst)
        {
        }
    }
}
