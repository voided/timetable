﻿using osu.Framework.Allocation;
using osu.Framework.Graphics.Cursor;
using osu.Framework.Graphics.UserInterface;
using timetable.Game.Tasks.Stores;
using timetable.Game.Tasks.Types;

namespace timetable.Game.UI.Tasks
{
    public abstract class DrawableTaskBase : DragDropItem<Task>, IHasContextMenu
    {
        [Resolved(canBeNull: true)]
        private TaskStore store { get; set; }

        private Task model;

        public override Task Model
        {
            get => model;
            set
            {
                model = value;

                if (model != null)
                    UpdateDisplay();
            }
        }

        public MenuItem[] ContextMenuItems => new[]
        {
            new MenuItem("Delete", () => store?.Delete(Model))
        };

        protected override void Update()
        {
            base.Update();

            if (store == null && Model != null)
                UpdateDisplay();
        }

        protected override void LoadComplete()
        {
            base.LoadComplete();

            if (store != null)
                store.ItemUpdated += updateDisplay;
        }

        protected override void Dispose(bool isDisposing)
        {
            if (store != null)
                store.ItemUpdated -= updateDisplay;

            base.Dispose(isDisposing);
        }

        private void updateDisplay(Task t)
        {
            if (Model == t) UpdateDisplay();
        }

        protected abstract void UpdateDisplay();
    }
}
