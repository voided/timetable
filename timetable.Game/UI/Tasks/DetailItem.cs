﻿using System;
using kyoseki.UI.Components.Theming;
using osu.Framework.Allocation;
using osu.Framework.Graphics;
using osu.Framework.Graphics.Containers;
using osu.Framework.Graphics.Sprites;
using osu.Framework.Localisation;
using osuTK;
using timetable.Game.UI.Theming;

namespace timetable.Game.UI.Tasks
{
    public class DetailItem : CompositeDrawable, IHasText
    {
        private const int spacing = 10;
        private const int icon_size = 15;
        private const int font_size = 15;

        public static int TotalHeight => Math.Max(icon_size, font_size);

        public LocalisableString Text
        {
            get => text.Text;
            set => text.Text = value;
        }

        public IconUsage Icon
        {
            get => icon.Icon;
            set => icon.Icon = value;
        }

        private readonly SpriteText text;
        private readonly SpriteIcon icon;

        public DetailItem()
        {
            Height = TotalHeight;

            Anchor = Anchor.TopLeft;
            Origin = Anchor.TopLeft;

            InternalChildren = new Drawable[]
            {
                icon = new SpriteIcon
                {
                    Anchor = Anchor.CentreLeft,
                    Origin = Anchor.CentreLeft,
                    Size = new Vector2(icon_size)
                },
                text = new SpriteText
                {
                    Anchor = Anchor.CentreLeft,
                    Origin = Anchor.CentreLeft,
                    Font = FontUsage.Default.With(size: font_size),
                    Truncate = true,
                    Padding = new MarginPadding { Left = icon_size + spacing }
                }
            };
        }

        [ThemeLoader]
        protected void LoadTheme(TimetableTheme theme, bool fade)
        {
            text.TweenOrSet(nameof(text.Colour), theme.ForegroundColour, fade);
        }

        [BackgroundDependencyLoader(true)]
        private void load(ThemeContainer themeContainer) => this.RegisterTo<TimetableTheme>(themeContainer);
    }
}
