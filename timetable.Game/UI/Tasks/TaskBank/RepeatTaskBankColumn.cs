﻿using System;
using timetable.Game.Tasks;
using timetable.Game.Tasks.Types;

namespace timetable.Game.UI.Tasks.TaskBank
{
    public class RepeatTaskBankColumn : TaskBankColumn
    {
        protected override string Title => "Repeatable";

        public override int Priority => 1;

        public override bool Accepts(Task t) => t is RepeatableTask;

        protected override Task CreateTask() => new RepeatableTask
        {
            RepeatRule = new RepeatRule(DateTime.Today, RepeatType.Interval, new TimeSpan(1, 0, 0, 0))
        };
    }
}
