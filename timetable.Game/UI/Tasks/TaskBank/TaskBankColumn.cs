﻿using System.Linq;
using kyoseki.UI.Components;
using kyoseki.UI.Components.Buttons;
using kyoseki.UI.Components.Theming;
using osu.Framework.Allocation;
using osu.Framework.Graphics;
using osu.Framework.Graphics.Containers;
using osu.Framework.Graphics.Shapes;
using osu.Framework.Graphics.Sprites;
using osu.Framework.Input.Events;
using osuTK;
using timetable.Game.Tasks.Types;
using timetable.Game.UI.Overlays;
using timetable.Game.UI.Theming;

namespace timetable.Game.UI.Tasks.TaskBank
{
    public abstract class TaskBankColumn : CompositeDrawable, IDropContainer<Task>
    {
        private const int width = DrawableTask.WIDTH + 30;
        private const int label_height = 40;

        [Resolved(canBeNull: true)]
        private TaskEditor editor { get; set; }

        protected abstract string Title { get; }
        public virtual int Priority => 0;

        private Box background;
        private SpriteText label;
        internal FillFlowDropContainer<DrawableTask, Task> DropContainer;

        public bool CanDrop(DragDropItem<Task> item) => false;

        public (Drawable, Vector2) GetInsertPosition(Task model) => DropContainer.GetInsertPosition(model);

        public void OnDrag(DragDropItem<Task> item) => DropContainer.OnDrag(item);

        public void OnDragFinalize(DragDropItem<Task> item) => DropContainer.OnDragFinalize(item);

        public DragDropItem<Task> OnDrop(Task model) => DropContainer.OnDrop(model);

        public bool Contains(DragDropItem<Task> item) => DropContainer.Children.Contains(item);

        public abstract bool Accepts(Task t);

        [ThemeLoader]
        protected void LoadTheme(TimetableTheme theme, bool fade)
        {
            background.TweenOrSet(nameof(background.Colour), theme.Background4, fade);

            label.Font = theme.DefaultFont.With(weight: "Bold", size: 24);
            label.TweenOrSet(nameof(label.Colour), theme.Content1, fade);
        }

        [BackgroundDependencyLoader(true)]
        private void load(ThemeContainer themeContainer)
        {
            RelativeSizeAxes = Axes.Y;
            Width = width;

            Masking = true;
            CornerRadius = DrawableTask.CORNER_RADIUS;

            InternalChildren = new Drawable[]
            {
                background = new Box
                {
                    RelativeSizeAxes = Axes.Both
                },
                new Container
                {
                    Name = "title container",
                    RelativeSizeAxes = Axes.X,
                    Height = label_height,
                    Child = label = new SpriteText
                    {
                        RelativeSizeAxes = Axes.X,
                        Anchor = Anchor.CentreLeft,
                        Origin = Anchor.CentreLeft,
                        Padding = new MarginPadding { Horizontal = 15 },
                        Truncate = true,
                        Text = Title
                    }
                },
                new Container
                {
                    Name = "content container",
                    RelativeSizeAxes = Axes.Both,
                    Padding = new MarginPadding { Top = label_height },
                    Children = new Drawable[]
                    {
                        new DragScrollContainer
                        {
                            RelativeSizeAxes = Axes.Both,
                            Padding = new MarginPadding
                            {
                                Top = DrawableTask.SHADOW_RADIUS,
                                Bottom = label_height
                            },
                            Child = DropContainer = new FillFlowDropContainer<DrawableTask, Task>
                            {
                                RelativeSizeAxes = Axes.X,
                                AutoSizeAxes = Axes.Y,
                                Direction = FillDirection.Vertical,
                                Spacing = new Vector2(10)
                            }
                        },
                        new IconButton
                        {
                            Size = new Vector2(40),
                            Anchor = Anchor.BottomRight,
                            Origin = Anchor.BottomRight,
                            Masking = true,
                            Margin = new MarginPadding(10),
                            CornerRadius = 20,
                            Icon = FontAwesome.Solid.Plus,
                            IconSize = new Vector2(0.4f),
                            Action = () =>
                            {
                                if (editor == null) return;

                                editor.Task = CreateTask();
                                editor.Show();
                            }
                        }
                    }
                }
            };

            this.RegisterTo<TimetableTheme>(themeContainer);
        }

        protected abstract Task CreateTask();

        public void Add(DrawableTask d) => Schedule(() => DropContainer.Add(d));

        private class DragScrollContainer : KyosekiScrollContainer
        {
            protected override bool OnDragStart(DragStartEvent e) => false;

            protected override bool OnMouseDown(MouseDownEvent e) => false;
        }
    }
}
