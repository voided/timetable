﻿using timetable.Game.Tasks.Types;

namespace timetable.Game.UI.Tasks.TaskBank
{
    public class GeneralTaskBankColumn : TaskBankColumn
    {
        protected override string Title => "General";

        public override bool Accepts(Task t) => true;

        protected override Task CreateTask() => new GeneralTask();
    }
}
