﻿using System;
using System.Collections.Generic;
using System.Linq;
using osu.Framework.Allocation;
using osu.Framework.Graphics;
using osu.Framework.Graphics.Containers;
using osu.Framework.Logging;
using osuTK;
using timetable.Game.Tasks.Stores;
using timetable.Game.Tasks.Types;
using timetable.Game.UI.Overlays;

namespace timetable.Game.UI.Tasks.TaskBank
{
    public class TaskBank : CompositeDrawable, IDropContainer<Task>
    {
        [Resolved(canBeNull: true)]
        private TaskEditor editor { get; set; }

        [Resolved(canBeNull: true)]
        private TaskStore store { get; set; }

        private readonly List<TaskBankColumn> cols = new List<TaskBankColumn>();

        public IEnumerable<TaskBankColumn> Columns
        {
            get => cols;
            set
            {
                cols.Clear();
                cols.AddRange(value);

                colFlow.Children = cols;
            }
        }

        private readonly FillFlowContainer colFlow;

        public TaskBank()
        {
            RelativeSizeAxes = Axes.Both;

            Padding = new MarginPadding(50);

            InternalChild = colFlow = new FillFlowContainer
            {
                RelativeSizeAxes = Axes.Y,
                AutoSizeAxes = Axes.X,
                Spacing = new Vector2(18),
                Direction = FillDirection.Horizontal
            };
        }

        public void Add(DrawableTask d)
        {
            d.Action = () => openTaskEditor(d.Model);

            getDst(d.Model).Add(d);
        }

        public bool CanDrop(DragDropItem<Task> item) => cols.Any(c => c.Accepts(item.Model));

        public (Drawable, Vector2) GetInsertPosition(Task model) => getDst(model).GetInsertPosition(model);

        public void OnDrag(DragDropItem<Task> item) => getDst(item.Model).OnDrag(item);

        public void OnDragFinalize(DragDropItem<Task> item)
        {
            getDst(item.Model).OnDragFinalize(item);

            ((DrawableTask)item).Action = null;
        }

        public DragDropItem<Task> OnDrop(Task model)
        {
            var d = getDst(model).OnDrop(model);

            ((DrawableTask)d).Action = () => openTaskEditor(model);

            return d;
        }

        public bool Contains(DragDropItem<Task> item) => cols.Any(c => c.Contains(item));

        protected override void LoadComplete()
        {
            base.LoadComplete();

            if (store == null)
                return;

            store.ItemCreated += taskCreated;
            store.Items.ForEach(taskCreated);

            store.ItemUpdated += taskUpdated;
            store.ItemDeleted += removeTask;

            store.TaskQueued += removeTask;
            store.TaskEnded += taskEnded;
        }

        protected override void Dispose(bool isDisposing)
        {
            base.Dispose(isDisposing);

            if (store == null)
                return;

            store.ItemCreated -= taskCreated;
            store.ItemUpdated -= taskUpdated;
            store.ItemDeleted -= removeTask;

            store.TaskQueued -= removeTask;
            store.TaskEnded -= taskEnded;
        }

        private void taskCreated(Task task)
        {
            if (task is RunnableTask rt && (rt.Complete != null || store.GetState(rt) != TaskState.Stopped))
                return;

            addTask(task);
        }

        private void taskUpdated(Task task)
        {
            if (!(task is RunnableTask rt))
                return;

            if (rt.Complete == null)
            {
                if (!containsTask(task))
                {
                    addTask(task);
                }
            }
            else
            {
                removeTask(task);
            }
        }

        private void taskEnded(RunnableTask task)
        {
            if (task.Complete == null)
                addTask(task);
        }

        private void addTask(Task task)
        {
            if (containsTask(task))
            {
                Logger.Log($"Attempted to add task {task.Id} to {nameof(TaskBank)} but it's already been added",
                    LoggingTarget.Information,
                    LogLevel.Error);

                return;
            }

            Add(new DrawableTask
            {
                Model = task
            });
        }

        private bool containsTask(Task task) => cols.Any(col => col.DropContainer.Any(child => child.Model == task));

        private void removeTask(Task task) => cols.ForEach(col =>
        {
            foreach (var child in col.DropContainer.Where(child => child.Model == task))
            {
                child.FadeOut(200).Expire();
            }
        });

        private void openTaskEditor(Task model)
        {
            if (editor == null) return;

            editor.Task = model;
            editor.Show();
        }

        private TaskBankColumn getDst(Task model)
        {
            var dst = cols.OrderByDescending(c => c.Priority).FirstOrDefault(c => c.Accepts(model));

            if (dst == null)
                throw new Exception("No column can accept this drawable!");

            return dst;
        }
    }
}
