﻿using System;
using System.Collections.Generic;
using System.Linq;
using kyoseki.UI.Components;
using osu.Framework.Graphics;
using osu.Framework.Graphics.Containers;
using osu.Framework.Graphics.Sprites;
using timetable.Game.Tasks.Types;

namespace timetable.Game.UI.Tasks
{
    public class CompletedTaskListing : CompositeDrawable
    {
        private readonly FillFlowContainer<ListDay> listFlow;

        public CompletedTaskListing()
        {
            InternalChild = new KyosekiScrollContainer
            {
                RelativeSizeAxes = Axes.Both,
                Child = listFlow = new FillFlowContainer<ListDay>
                {
                    RelativeSizeAxes = Axes.X,
                    AutoSizeAxes = Axes.Y,
                    Direction = FillDirection.Vertical
                }
            };
        }

        public IEnumerable<DrawableCompletedTask> Add(RunnableTask task)
        {
            if (!task.Complete.HasValue)
                throw new Exception("Task must be complete to be added to this container.");

            var drawables = new List<DrawableCompletedTask>();
            var dates = new List<DateTime> { task.Complete.Value.Date };

            foreach (var f in task.WorkTime)
            {
                var date = f.Start.Date;
                if (!dates.Contains(date))
                    dates.Add(date);
            }

            foreach (var date in dates)
            {
                var dst = addList(date);

                var dct = new DrawableCompletedTask
                {
                    Model = task
                };

                dst.Add(dct);
                drawables.Add(dct);
            }

            return drawables;
        }

        public void ExpireTask(DrawableCompletedTask drawable)
        {
            var container = listFlow.FirstOrDefault(l => l.Contains(drawable));
            if (container == null) return;

            if (container.Count == 1)
                container.FadeOut(150).Expire();
            else
                drawable.FadeOut(150).Expire();
        }

        private ListDay addList(DateTime date)
        {
            var existing = listFlow.Children.FirstOrDefault(f => f.Date == date);

            if (existing != null) return existing;

            var day = new ListDay(date);

            listFlow.Add(day);

            var children = listFlow.Children.ToList();
            children.Sort();

            for (int i = 0; i < children.Count; i++)
                listFlow.SetLayoutPosition(children[i], i);

            return day;
        }

        private class ListDay : Container<DrawableCompletedTask>, IComparable<ListDay>
        {
            private const int header_size = 24;

            public DateTime Date { get; }

            protected override Container<DrawableCompletedTask> Content { get; }

            public ListDay(DateTime date)
            {
                Date = date;

                RelativeSizeAxes = Axes.X;
                AutoSizeAxes = Axes.Y;

                InternalChildren = new Drawable[]
                {
                    new ThemedText(20)
                    {
                        Text = date.ToString("yyyy MMMM dd")
                    },
                    Content = new FillFlowContainer<DrawableCompletedTask>
                    {
                        RelativeSizeAxes = Axes.X,
                        AutoSizeAxes = Axes.Y,
                        Direction = FillDirection.Vertical,
                        Padding = new MarginPadding { Top = header_size }
                    }
                };
            }

            public int CompareTo(ListDay other) => other.Date.CompareTo(Date);
        }
    }

    public class DrawableCompletedTask : DrawableTaskBase
    {
        private readonly SpriteText text;

        public DrawableCompletedTask()
        {
            AutoSizeAxes = Axes.Both;
            InternalChild = text = new ThemedText(16);
        }

        protected override void UpdateDisplay()
        {
            text.Text = string.IsNullOrEmpty(Model.Name) ? "<no name>" : Model.Name;
        }
    }
}
