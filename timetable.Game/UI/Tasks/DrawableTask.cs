﻿using System;
using kyoseki.UI.Components.Theming;
using osu.Framework.Allocation;
using osu.Framework.Graphics;
using osu.Framework.Graphics.Colour;
using osu.Framework.Graphics.Containers;
using osu.Framework.Graphics.Effects;
using osu.Framework.Graphics.Shapes;
using osu.Framework.Graphics.Sprites;
using osu.Framework.Input.Events;
using osuTK;
using timetable.Game.Tasks.Types;
using timetable.Game.UI.Theming;

namespace timetable.Game.UI.Tasks
{
    public class DrawableTask : DrawableTaskBase
    {
        public const int WIDTH = 230;
        public const int CORNER_RADIUS = 20;
        public const int SHADOW_RADIUS = 10;

        private const int expand_small = 150;
        private const int base_height = 90;

        private bool expanded = true;

        public bool Expanded
        {
            get => expanded;
            set => SetExpanded(value);
        }

        public ColourInfo ShadowColour { get; set; }

        public Action Action;

        private readonly Container content;

        private readonly Container details;

        private readonly SpriteText groupText;
        private readonly SpriteText nameText;

        private readonly DetailItem dueDetail;
        private readonly DetailItem spentDetail;

        private readonly Box bgTop;
        private readonly Box bgBottom;

        public DrawableTask()
        {
            Width = WIDTH;
            Height = expand_small;

            Anchor = Anchor.TopCentre;
            Origin = Anchor.TopCentre;

            const int group_height = 12;
            const int text_height = 15;

            InternalChild = content = new Container
            {
                Name = "scaling content",
                RelativeSizeAxes = Axes.Both,
                Anchor = Anchor.Centre,
                Origin = Anchor.Centre,
                Masking = true,
                CornerRadius = CORNER_RADIUS,
                Children = new Drawable[]
                {
                    details = new Container
                    {
                        RelativeSizeAxes = Axes.X,
                        Height = expand_small,
                        Masking = true,
                        CornerRadius = CORNER_RADIUS,
                        Children = new Drawable[]
                        {
                            bgBottom = new Box
                            {
                                RelativeSizeAxes = Axes.Both
                            },
                            new Container
                            {
                                RelativeSizeAxes = Axes.Both,
                                Padding = new MarginPadding { Top = base_height + 8, Horizontal = 20 },
                                Children = new Drawable[]
                                {
                                    dueDetail = new DetailItem
                                    {
                                        RelativeSizeAxes = Axes.X
                                    },
                                    spentDetail = new DetailItem
                                    {
                                        RelativeSizeAxes = Axes.X,
                                        Icon = FontAwesome.Regular.Clock,
                                        Y = DetailItem.TotalHeight + 10
                                    }
                                }
                            }
                        }
                    },
                    new Container
                    {
                        RelativeSizeAxes = Axes.X,
                        Height = base_height,
                        Masking = true,
                        CornerRadius = CORNER_RADIUS,
                        Children = new Drawable[]
                        {
                            bgTop = new Box
                            {
                                RelativeSizeAxes = Axes.Both
                            },
                            new FillFlowContainer
                            {
                                RelativeSizeAxes = Axes.Both,
                                Direction = FillDirection.Vertical,
                                Spacing = new Vector2(0, 5),
                                Padding = new MarginPadding(12),
                                Children = new Drawable[]
                                {
                                    new Container
                                    {
                                        RelativeSizeAxes = Axes.Both,
                                        Height = 0.8f,
                                        Masking = true,
                                        Child = nameText = new SpriteText
                                        {
                                            RelativeSizeAxes = Axes.X,
                                            Font = FontUsage.Default.With(size: text_height)
                                        }
                                    },
                                    groupText = new SpriteText
                                    {
                                        RelativeSizeAxes = Axes.Both,
                                        Height = 0.2f,
                                        Truncate = true,
                                        Font = FontUsage.Default.With(size: group_height, weight: "Bold")
                                    }
                                }
                            }
                        }
                    }
                }
            };
        }

        public void SetExpanded(bool expand, bool animate = true, bool resizeThis = true)
        {
            if (expand)
            {
                var duration = animate ? 300 : 0;

                if (resizeThis) this.ResizeHeightTo(expand_small, duration, Easing.OutQuint);
                details.ResizeHeightTo(expand_small, duration, Easing.OutQuint);
            }
            else
            {
                var duration = animate ? 250 : 0;

                if (resizeThis) this.ResizeHeightTo(base_height, duration, Easing.OutExpo);
                details.ResizeHeightTo(base_height, duration, Easing.OutExpo);
            }

            expanded = expand;
        }

        public void SetHovering(bool hovering, bool animate = true)
        {
            const Easing easing = Easing.OutQuad;
            const float duration = 200;

            if (hovering)
            {
                const float scale_hovering = 1.02f;

                if (animate)
                {
                    content.TweenEdgeEffectTo(edgeEffect(SHADOW_RADIUS), duration, easing)
                           .ScaleTo(scale_hovering, duration, easing);
                }
                else
                {
                    content.EdgeEffect = edgeEffect(SHADOW_RADIUS);
                    content.Scale = new Vector2(scale_hovering);
                }
            }
            else
            {
                if (animate)
                {
                    content.TweenEdgeEffectTo(edgeEffect(0), duration, easing)
                           .ScaleTo(1, duration, easing);
                }
                else
                {
                    content.EdgeEffect = edgeEffect(0);
                    content.Scale = Vector2.One;
                }
            }
        }

        protected override bool OnHover(HoverEvent e)
        {
            SetHovering(true);
            return base.OnHover(e);
        }

        protected override void OnHoverLost(HoverLostEvent e)
        {
            SetHovering(false);
            base.OnHoverLost(e);
        }

        private EdgeEffectParameters edgeEffect(float radius) => new EdgeEffectParameters
        {
            Colour = ShadowColour,
            Type = EdgeEffectType.Shadow,
            Radius = radius,
        };

        protected override void UpdateDisplay()
        {
            nameText.Text = Model.Name;
            groupText.Text = Model.Group;

            switch (Model)
            {
                case RepeatableTask rt:
                    dueDetail.Text = rt.RepeatRule.ToString(true);
                    dueDetail.Icon = FontAwesome.Solid.Redo;
                    break;

                case RepeatChildTask _:
                    dueDetail.Text = "Repeat task";
                    dueDetail.Icon = FontAwesome.Solid.Redo;
                    break;

                case GeneralTask gt:
                    dueDetail.Text = gt.Due.HasValue ? $"Due {gt.Due.Value}" : "No due date";
                    dueDetail.Icon = FontAwesome.Regular.Calendar;
                    break;
            }

            if (Model is RunnableTask rut)
                spentDetail.Text = $"Spent {rut.WorkDuration}";
            else
                spentDetail.Text = "No time data";
        }

        protected override bool OnClick(ClickEvent e)
        {
            Action?.Invoke();

            return true;
        }

        public override void OnDrag(DragDropItem<Task> cursor)
        {
            var c = (DrawableTask)cursor;

            c.SetExpanded(true, false);
            c.SetExpanded(false);
            c.SetHovering(true, false);
        }

        public override void OnDrop(DragDropItem<Task> target, IDropContainer<Task> dst)
        {
            // TODO: Cancel animation when appropriate

            if (target == null)
                return;

            var t = (DrawableTask)target;

            t.SetExpanded(false, false, false);
            t.SetExpanded(true, resizeThis: false);
        }

        [ThemeLoader]
        protected void LoadTheme(TimetableTheme theme, bool fade)
        {
            nameText.TweenOrSet(nameof(nameText.Colour), theme.ForegroundColour, fade);
            groupText.TweenOrSet(nameof(groupText.Colour), theme.Link1, fade);
            bgTop.TweenOrSet(nameof(bgTop.Colour), theme.Background3, fade);
            bgBottom.TweenOrSet(nameof(bgBottom.Colour), theme.Background2, fade);

            this.TweenOrSet(nameof(ShadowColour), theme.Shadow, fade);
        }

        [BackgroundDependencyLoader(true)]
        private void load(ThemeContainer themeContainer)
        {
            this.RegisterTo<TimetableTheme>(themeContainer);
            content.EdgeEffect = edgeEffect(0);
        }
    }
}
