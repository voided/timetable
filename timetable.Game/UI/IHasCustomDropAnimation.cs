using osu.Framework.Graphics.Transforms;
using osuTK;

namespace timetable.Game.UI
{
    public interface IHasCustomDropAnimation<T>
    {
        TransformSequence<DragDropItem<T>> Animate(DragDropItem<T> item, Vector2 targetPosition);
    }
}
