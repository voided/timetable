using kyoseki.UI.Components;
using osu.Framework.Allocation;
using osu.Framework.Graphics;
using osu.Framework.Screens;
using timetable.Game.Screens.Main;
using timetable.Game.Tasks.Types;
using timetable.Game.UI;
using timetable.Game.UI.Overlays;
using timetable.Game.UI.Tasks;
using timetable.Game.UI.Tasks.TaskBank;

namespace timetable.Game.Screens
{
    public class MainScreen : Screen
    {
        [Cached]
        private MainToolbar toolbar { get; }

        [Cached]
        private TaskEditor editor { get; }

        public MainScreen()
        {
            InternalChild = new KyosekiContextMenuContainer
            {
                RelativeSizeAxes = Axes.Both,
                Children = new Drawable[]
                {
                    new Background
                    {
                        RelativeSizeAxes = Axes.Both
                    },
                    new TaskDragDropContainer
                    {
                        RelativeSizeAxes = Axes.Both,
                        Children = new Drawable[]
                        {
                            toolbar = new MainToolbar(),
                            new TaskBank
                            {
                                Anchor = Anchor.TopLeft,
                                Origin = Anchor.TopLeft,
                                Columns = new TaskBankColumn[]
                                {
                                    new GeneralTaskBankColumn(),
                                    new RepeatTaskBankColumn()
                                }
                            }
                        }
                    },
                    editor = new TaskEditor()
                }
            };
        }

        private class TaskDragDropContainer : DragDropContainer<Task>
        {
            protected override DragDropItem<Task> CreateCursor() => new DrawableTask();

            private MainToolbar toolbar;

            [BackgroundDependencyLoader]
            private void load(MainToolbar toolbar)
            {
                this.toolbar = toolbar;

                OnCursorActive += onCursorActive;
                OnCursorInactive += onCursorInactive;
            }

            private void onCursorActive(DragDropItem<Task> cursor) => toolbar.SetDragging(true);

            private void onCursorInactive(DragDropItem<Task> cursor, DragDropItem<Task> item) => toolbar.SetDragging(false);
        }
    }
}
