﻿using kyoseki.UI.Components.Buttons;
using osu.Framework.Allocation;
using osu.Framework.Graphics;
using osu.Framework.Graphics.Containers;
using osu.Framework.Graphics.Sprites;
using osuTK;
using timetable.Game.Tasks.Stores;

namespace timetable.Game.Screens.Main
{
    public class TaskController : CompositeDrawable
    {
        public const int BUTTON_SIZE = 20;

        [Resolved(canBeNull: true)]
        private TaskStore store { get; set; }

        private readonly TaskControllerButton pauseButton;

        public TaskController()
        {
            AutoSizeAxes = Axes.Both;

            AddInternal(new FillFlowContainer
            {
                AutoSizeAxes = Axes.Both,
                Direction = FillDirection.Horizontal,
                Spacing = new Vector2(15, 0),
                Children = new Drawable[]
                {
                    new TaskControllerButton
                    {
                        Icon = FontAwesome.Solid.Check,
                        Action = () =>
                        {
                            var task = store?.FocusTask;
                            if (task == null) return;

                            store.MarkComplete(task);
                        }
                    },
                    pauseButton = new TaskControllerButton
                    {
                        Icon = FontAwesome.Solid.Pause,
                        Action = () =>
                        {
                            var task = store?.FocusTask;
                            if (task == null) return;

                            if (store.GetState(task) == TaskState.Paused)
                                store.Start(task);
                            else
                                store.Pause(task);
                        }
                    },
                    new TaskControllerButton
                    {
                        Icon = FontAwesome.Solid.Times,
                        Action = () =>
                        {
                            var task = store?.FocusTask;
                            if (task == null || store.GetState(task) == TaskState.Stopped) return;

                            store.End(task);
                        }
                    }
                }
            });
        }

        protected override void Update()
        {
            base.Update();

            if (store == null) return;

            pauseButton.Icon = store.GetState(store.FocusTask) == TaskState.Paused ? FontAwesome.Solid.Play : FontAwesome.Solid.Pause;
        }

        private class TaskControllerButton : IconButton
        {
            public TaskControllerButton()
            {
                Size = new Vector2(BUTTON_SIZE);
                IconSize = new Vector2(0.5f);

                Masking = true;
                CornerRadius = BUTTON_SIZE / 2f;

                Anchor = Anchor.Centre;
                Origin = Anchor.Centre;
            }
        }
    }
}
