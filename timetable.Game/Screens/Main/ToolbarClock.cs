using System;
using kyoseki.UI.Components;
using osu.Framework.Graphics;
using osu.Framework.Graphics.Containers;
using osu.Framework.Graphics.Sprites;

namespace timetable.Game.Screens.Main
{
    internal class ToolbarClock : CompositeDrawable
    {
        private const float width = 60;
        private const string separator = ":";

        private int? lastSecond;

        private readonly SpriteText hour;
        private readonly SpriteText hourColon;
        private readonly SpriteText minute;
        private readonly SpriteText minuteColon;
        private readonly SpriteText second;

        public ToolbarClock()
        {
            AutoSizeAxes = Axes.Y;
            Width = width;

            InternalChild = new FillFlowContainer
            {
                AutoSizeAxes = Axes.Both,
                Anchor = Anchor.CentreLeft,
                Origin = Anchor.CentreLeft,
                Direction = FillDirection.Full,
                Children = new Drawable[]
                {
                    hour = new ThemedText(MainToolbar.FONT_SIZE),
                    hourColon = new ThemedText(MainToolbar.FONT_SIZE)
                    {
                        AlwaysPresent = true,
                        Text = separator
                    },
                    minute = new ThemedText(MainToolbar.FONT_SIZE),
                    minuteColon = new ThemedText(MainToolbar.FONT_SIZE)
                    {
                        AlwaysPresent = true,
                        Text = separator
                    },
                    second = new ThemedText(MainToolbar.FONT_SIZE)
                }
            };

            updateDate(DateTime.Now);
        }

        private void updateDate(DateTime now)
        {
            hour.Text = now.Hour.ToString("D2");
            minute.Text = now.Minute.ToString("D2");
            second.Text = now.Second.ToString("D2");

            var colonAlpha = now.Second % 2 == 0 ? 1 : 0;
            hourColon.Alpha = minuteColon.Alpha = colonAlpha;
        }

        protected override void Update()
        {
            base.Update();

            var date = DateTime.Now;

            if (date.Second != lastSecond)
            {
                lastSecond = date.Second;
                updateDate(date);
            }
        }
    }
}
