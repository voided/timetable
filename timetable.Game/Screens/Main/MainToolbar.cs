﻿using System;
using kyoseki.UI.Components;
using kyoseki.UI.Components.Buttons;
using kyoseki.UI.Components.Theming;
using osu.Framework.Allocation;
using osu.Framework.Graphics;
using osu.Framework.Graphics.Colour;
using osu.Framework.Graphics.Containers;
using osu.Framework.Graphics.Effects;
using osu.Framework.Graphics.Shapes;
using osu.Framework.Graphics.Sprites;
using osu.Framework.Graphics.Transforms;
using osuTK;
using timetable.Game.Tasks.Stores;
using timetable.Game.Tasks.Types;
using timetable.Game.UI;
using timetable.Game.UI.Theming;

namespace timetable.Game.Screens.Main
{
    public class MainToolbar : CompositeDrawable, IDropContainer<Task>, IHasCustomDropAnimation<Task>
    {
        public const float FONT_SIZE = 20;

        private const float height = 36;
        private const float border_radius = 8;
        private const float border_thickness = 4;
        private const float spacing = 12;

        [Resolved(canBeNull: true)]
        private TaskStore store { get; set; }

        private Box background;
        private Drawable content;
        private Drawable dragPrompt;

        private bool isDragging;
        public ColourInfo GlowColour { get; set; }

        [ThemeLoader]
        protected void LoadTheme(TimetableTheme theme, bool fade)
        {
            background.TweenOrSet(nameof(background.Colour), theme.Background3, fade);
            this.TweenOrSet(nameof(BorderColour), theme.Background2, fade);
            this.TweenOrSet(nameof(GlowColour), theme.Background1, fade);
        }

        [BackgroundDependencyLoader(true)]
        private void load(ThemeContainer themeContainer)
        {
            Anchor = Anchor.TopCentre;
            Origin = Anchor.TopCentre;

            AutoSizeAxes = Axes.X;

            Masking = true;
            CornerRadius = border_radius;
            BorderThickness = border_thickness;

            Y = -border_radius;
            Height = height + border_radius + border_thickness;
            Padding = new MarginPadding { Top = border_radius };

            InternalChildren = new Drawable[]
            {
                background = new Box
                {
                    RelativeSizeAxes = Axes.Both
                },
                new Container
                {
                    AutoSizeAxes = Axes.X,
                    Height = height,
                    Margin = new MarginPadding
                    {
                        Bottom = border_thickness,
                        Horizontal = border_thickness + spacing
                    },
                    Children = new[]
                    {
                        content = new FillFlowContainer
                        {
                            AlwaysPresent = true, // For layout of dragPrompt
                            Name = "toolbar content",
                            AutoSizeAxes = Axes.X,
                            RelativeSizeAxes = Axes.Y,
                            Direction = FillDirection.Horizontal,
                            Children = new Drawable[]
                            {
                                new ToolbarButton
                                {
                                    Icon = FontAwesome.Solid.Check
                                },
                                new ToolbarButton
                                {
                                    Icon = FontAwesome.Solid.Music
                                },
                                new ToolbarButton
                                {
                                    Icon = FontAwesome.Solid.Cog
                                },
                                new Separator(),
                                new ToolbarClock
                                {
                                    Anchor = Anchor.Centre,
                                    Origin = Anchor.Centre
                                }
                            }
                        },
                        dragPrompt = new Container
                        {
                            RelativeSizeAxes = Axes.Both,
                            Alpha = 0,
                            Child = new ThemedText(18)
                            {
                                Anchor = Anchor.Centre,
                                Origin = Anchor.Centre,
                                Text = "drag here to start!"
                            }
                        }
                    }
                }
            };

            this.RegisterTo<TimetableTheme>(themeContainer);

            EdgeEffect = edgeEffect(0);
        }

        public void SetDragging(bool isDragging)
        {
            const float edge_duration = 800;
            const Easing easing = Easing.OutSine;

            const float fade_duration = 300;

            if (this.isDragging == isDragging)
                return;

            if (isDragging)
            {
                TweenEdgeEffectTo(edgeEffect(20), edge_duration, easing)
                    .Then()
                    .Delay(edge_duration / 2)
                    .Append(_ => TweenEdgeEffectTo(edgeEffect(1), edge_duration, easing))
                    .Then()
                    .Delay(edge_duration / 2)
                    .Loop();
            }
            else
            {
                ClearTransforms();
                TweenEdgeEffectTo(edgeEffect(0), edge_duration / 2, easing);
            }

            content.FadeTo(isDragging ? 0 : 1, fade_duration, easing);
            dragPrompt.FadeTo(isDragging ? 1 : 0, fade_duration, easing);

            this.isDragging = isDragging;
        }

        private EdgeEffectParameters edgeEffect(float radius) => new EdgeEffectParameters
        {
            Colour = GlowColour,
            Type = EdgeEffectType.Glow,
            Radius = radius
        };

        public bool CanDrop(DragDropItem<Task> item)
        {
            if (item.Model is RunnableTask)
                return true;

            store?.Start(item.Model);

            return false;
        }

        public (Drawable, Vector2) GetInsertPosition(Task model) => (this, Vector2.Zero);

        public DragDropItem<Task> OnDrop(Task model)
        {
            store?.Start(model);
            return null;
        }

        public void OnDrag(DragDropItem<Task> item) =>
            throw new InvalidOperationException("This container does not support dragging.");

        public void OnDragFinalize(DragDropItem<Task> item) =>
            throw new InvalidOperationException("This container does not support dragging.");

        public bool Contains(DragDropItem<Task> item) => false;

        public TransformSequence<DragDropItem<Task>> Animate(DragDropItem<Task> item, Vector2 targetPosition) =>
            item.FadeOut(300, Easing.OutQuad);

        private class ToolbarButton : IconButton<TimetableTheme>
        {
            private const float size = 28;

            public ToolbarButton()
            {
                Anchor = Anchor.Centre;
                Origin = Anchor.Centre;

                Size = new Vector2(size);
                IconSize = new Vector2(0.5f);

                CornerRadius = size / 2f;
                Masking = true;
            }

            protected override void LoadTheme(TimetableTheme theme, bool fade)
            {
                Background.TweenOrSet(nameof(Background.Colour), theme.Background3, fade);
                this.TweenOrSet(nameof(FlashColour), theme.Link2, fade);
                SpriteIcon.TweenOrSet(nameof(SpriteIcon.Colour), theme.Content1, fade);
            }
        }

        private class Separator : CompositeDrawable
        {
            private const float sep_height = 24;
            private const float sep_width = 1;

            private readonly Box box;

            public Separator()
            {
                Anchor = Anchor.Centre;
                Origin = Anchor.Centre;

                Height = sep_height;
                Width = sep_width;

                Margin = new MarginPadding { Horizontal = spacing };

                InternalChild = box = new Box
                {
                    RelativeSizeAxes = Axes.Both
                };
            }

            [ThemeLoader]
            protected void LoadTheme(TimetableTheme theme, bool fade)
            {
                box.TweenOrSet(nameof(box.Colour), theme.ForegroundColour, fade);
            }

            [BackgroundDependencyLoader(true)]
            private void load(ThemeContainer themeContainer) => this.RegisterTo<TimetableTheme>(themeContainer);
        }
    }
}
