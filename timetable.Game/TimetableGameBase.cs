using osu.Framework.Allocation;
using osu.Framework.Graphics;
using osu.Framework.Graphics.Containers;
using osu.Framework.IO.Stores;
using timetable.Game.Input.Bindings;
using timetable.Game.UI.Theming;
using timetable.Resources;
using DrawableExtensions = kyoseki.UI.Components.Theming.DrawableExtensions;

namespace timetable.Game
{
    public class TimetableGameBase : osu.Framework.Game
    {
        protected override Container<Drawable> Content { get; }

        protected TimetableGameBase()
        {
            DrawableExtensions.DefaultTheme = new TimetableTheme();
            base.Content.Add(new DrawSizePreservingFillContainer
            {
                Child = Content = new GlobalActionContainer { RelativeSizeAxes = Axes.Both }
            });
        }

        [BackgroundDependencyLoader]
        private void load()
        {
            Resources.AddStore(new DllResourceStore(typeof(TimetableResources).Assembly));
            loadFonts();
        }

        private void loadFonts()
        {
            AddFont(Resources, @"Fonts/Manrope");
            AddFont(Resources, @"Fonts/Manrope-Bold");
            AddFont(Resources, @"Fonts/Manrope-Italic");
            AddFont(Resources, @"Fonts/Manrope-BoldItalic");
        }
    }
}
