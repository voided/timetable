﻿using osu.Framework.Allocation;
using osu.Framework.Graphics;
using osu.Framework.Screens;
using timetable.Game.IO;
using timetable.Game.Screens;
using timetable.Game.Tasks;
using timetable.Game.Tasks.Stores;

namespace timetable.Game
{
    public class TimetableGame : TimetableGameBase
    {
        private ScreenStack screenStack;

        private TaskStore store;

        [BackgroundDependencyLoader]
        private void load()
        {
            // Add your top-level game components here.
            // A screen stack and sample screen has been provided for convenience, but you can replace it if you don't want to use screens.
            Child = screenStack = new ScreenStack { RelativeSizeAxes = Axes.Both };
        }

        private DependencyContainer dependencies;

        protected override IReadOnlyDependencyContainer CreateChildDependencies(IReadOnlyDependencyContainer parent)
            => dependencies = new DependencyContainer(base.CreateChildDependencies(parent));

        protected override void LoadComplete()
        {
            base.LoadComplete();

            var jsonStore = new FileJsonStore<StoreObject>(Host.Storage);

            dependencies.Cache(new GroupStore(jsonStore));

            dependencies.Cache(store = new TaskStore(jsonStore));
            AddInternal(store);

            screenStack.Push(new MainScreen());
        }

        protected override bool OnExiting()
        {
            store?.EndAll();
            return base.OnExiting();
        }
    }
}
