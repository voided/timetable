﻿using System;
using osu.Framework.Threading;
using timetable.Game.Tasks.Stores;
using timetable.Game.Tasks.Types;

namespace timetable.Game.Tasks
{
    /// <summary>
    /// A class responsible for managing the starting/pausing/stopping of a
    /// <see cref="TaskStore"/>'s <see cref="Task"/>s.
    /// </summary>
    public abstract class Timer
    {
        protected readonly TaskStore Store;
        protected readonly Scheduler Scheduler = new Scheduler();

        protected Timer(TaskStore store)
        {
            Store = store;
        }

        public virtual RunnableTask Start(Task task) => Store.Start(task, null, true);

        public virtual void Pause(RunnableTask task) => Store.Pause(task, true);

        public virtual void End(RunnableTask task) =>
            Store.End(task, true, Store.Find(task.Id) != null); // do not push time frame if task is deleted

        /// <summary>
        /// Returns the state of the task according to this Timer, or return null to fall back to TaskStore's state.
        /// </summary>
        public virtual TaskState? GetState(RunnableTask task) => null;

        /// <summary>
        /// Gets the current running time of the task as a string.
        /// </summary>
        public virtual string GetTime(RunnableTask task)
        {
            var start = Store.GetStart(task);

            if (start == null)
                return "<paused>";

            var timeSpan = DateTime.Now - start.Value;

            return timeSpan.ToString("G");
        }

        /// <summary>
        /// Called on every TaskStore update.
        /// </summary>
        public virtual void Update() => Scheduler.Update();
    }
}
