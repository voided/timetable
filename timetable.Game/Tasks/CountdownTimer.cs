﻿using System;
using System.Collections.Generic;
using osu.Framework.Threading;
using timetable.Game.Tasks.Stores;
using timetable.Game.Tasks.Types;

namespace timetable.Game.Tasks
{
    public class CountdownTimer : Timer
    {
        private const int countdown_ms = 5000;

        private readonly Dictionary<Guid, CountdownContext> waitingTasks = new Dictionary<Guid, CountdownContext>();

        public CountdownTimer(TaskStore store)
            : base(store)
        {
        }

        public override RunnableTask Start(Task task)
        {
            var queued = Store.Queue(task);

            if (queued == null) return null;

            var del = Scheduler.AddDelayed(() =>
            {
                Store.Start(queued, waitingTasks[queued.Id].Start.AddMilliseconds(5000), true);
                waitingTasks.Remove(queued.Id);
            }, countdown_ms);

            waitingTasks.Add(queued.Id, new CountdownContext(del));

            return queued;
        }

        public override void End(RunnableTask task)
        {
            base.End(task);

            if (waitingTasks.TryGetValue(task.Id, out CountdownContext ctx))
            {
                ctx.Delegate.Cancel();
                waitingTasks.Remove(task.Id);
            }
        }

        public override TaskState? GetState(RunnableTask task)
        {
            return waitingTasks.ContainsKey(task.Id)
                ? TaskState.Running
                : base.GetState(task);
        }

        public override string GetTime(RunnableTask task)
        {
            if (waitingTasks.TryGetValue(task.Id, out CountdownContext ctx))
            {
                var targetTime = ctx.Start.AddMilliseconds(countdown_ms);

                // if a bit past target start time then just return what the time should be
                if (DateTime.Now > targetTime) return (DateTime.Now - targetTime).ToString("G");

                var countdownTime = targetTime - DateTime.Now;

                return countdownTime.Seconds.ToString();
            }

            return base.GetTime(task);
        }

        private readonly struct CountdownContext
        {
            public DateTime Start { get; }

            public ScheduledDelegate Delegate { get; }

            public CountdownContext(ScheduledDelegate del)
            {
                Start = DateTime.Now;
                Delegate = del;
            }
        }
    }
}
