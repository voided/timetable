﻿using System;
using Newtonsoft.Json;

namespace timetable.Game.Tasks.Stores
{
    [JsonObject(MemberSerialization.OptIn)]
    public abstract class GuidObject
    {
        [JsonProperty("id")]
        public Guid Id { get; set; } = Guid.NewGuid();

        [JsonProperty("created")]
        public DateTime? Created { get; set; }
    }
}
