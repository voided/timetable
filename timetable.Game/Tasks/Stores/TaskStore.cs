﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using timetable.Game.IO;
using timetable.Game.Tasks.Types;

namespace timetable.Game.Tasks.Stores
{
    /// <summary>
    /// A data store responsible for managing tasks and their state.
    /// </summary>
    public class TaskStore : GuidObjectStore<Task>
    {
        private readonly JsonStore<StoreObject> store;

        public override List<Task> Items => store.Value.Tasks;

        public Timer Timer { get; set; }

        public event Action<RunnableTask> TaskQueued;
        public event Action<RunnableTask> TaskStarted;
        public event Action<RunnableTask> TaskPaused;
        public event Action<RunnableTask> TaskEnded;

        /// <summary>
        /// The first running task. Should be used when only a singular task should be handled
        /// (i.e. for the timer / controller / TaskDropContainer)
        /// </summary>
        public RunnableTask FocusTask { get; private set; }

        /// <summary>
        /// Stores data on currently running tasks.
        /// If the DateTime is null, then the task is paused.
        /// </summary>
        protected readonly Dictionary<Guid, DateTime?> RunningTasks = new Dictionary<Guid, DateTime?>();

        public TaskStore(JsonStore<StoreObject> store)
        {
            this.store = store;
            Timer = new CountdownTimer(this);
        }

        protected override void Save() => store.Save();

        protected override void PrepareDeletion(Task item)
        {
            // end the task if it is running
            if (item is RunnableTask rt && GetState(rt) != TaskState.Stopped)
                End(rt, push: false);

            if (item == FocusTask)
                FocusTask = null;
        }

        /// <summary>
        /// For the given <paramref name="task"/>, return the task that can be used in
        /// <see cref="Start"/>.
        /// (i.e. for repeatable tasks, return a valid existing/new child)
        /// </summary>
        private RunnableTask getRunnableTask(Task task)
        {
            if (task is RunnableTask rut) return rut;
            if (!(task is RepeatableTask rt)) return null;

            var parentRule = rt.RepeatRule;

            if (parentRule.StartDate > DateTime.Now)
                return null;

            var res = Items
                      .Where(t => t is RepeatChildTask rct && rct.Parent == task.Id && !rct.Complete.HasValue)
                      .OrderByDescending(t => t.Created).ToList();

            if (res.Any()) // find any existing task that should be used instead of a new one
            {
                foreach (var rct in res)
                {
                    var rule = ((RepeatChildTask)rct).RepeatRule;

                    switch (rule.Type)
                    {
                        case RepeatType.Interval:
                            Trace.Assert(rule.Interval.HasValue);

                            if (rule.StartDate > DateTime.Today) continue;

                            if (rule.IntervalValid(DateTime.Today))
                                return Start(rct, force: true);

                            break;

                        case RepeatType.OnDays:
                            Trace.Assert(rct.Created.HasValue);

                            if (DateTime.Today == rct.Created.Value.Date) // checking same day should be sufficient
                                return Start(rct, force: true);

                            break;
                    }
                }
            }

            // create a new task
            switch (parentRule.Type)
            {
                case RepeatType.Interval:
                    if (!parentRule.IntervalValid(DateTime.Today))
                        return null;

                    break;

                case RepeatType.OnDays:
                    if (!parentRule.DaysValid(DateTime.Today))
                        return null;

                    break;
            }

            RepeatChildTask newRct;

            // When the creation event for this task is fired,
            // we want it to show as Paused and not Stopped.
            // This is the only case where queueing a non-registered task is OK.
            Queue(newRct = rt.CreateRepeatChild(), true);

            Add(newRct);

            return newRct;
        }

        /// <summary>
        /// Queues any task to be started (i.e. start it paused).
        /// If the task is already queued, return it and do nothing.
        /// </summary>
        public RunnableTask Queue(Task task, bool force = false)
        {
            if (!force && !Contains(task.Id))
                throw new InvalidOperationException("Cannot queue a non-registered task by default.");

            var toStart = getRunnableTask(task);
            if (toStart == null) return null;

            if (toStart.Complete != null) // TODO
                throw new Exception("Cannot start a task that has been marked complete.");

            if (RunningTasks.ContainsKey(toStart.Id)) return toStart;

            RunningTasks.Add(toStart.Id, null);

            FocusTask ??= toStart;

            TaskQueued?.Invoke(toStart);

            return toStart;
        }

        /// <summary>
        /// Starts any registered task in this store or resumes it from paused state.
        /// </summary>
        /// <param name="task">The task to start.</param>
        /// <param name="start">The time to use. Defaults to current time.</param>
        /// <param name="force">Whether to bypass the <see cref="Timer"/>.</param>
        /// <returns>The task that started, or null if no task started.</returns>
        public RunnableTask Start(Task task, DateTime? start = null, bool force = false)
        {
            if (!Contains(task.Id))
                throw new InvalidOperationException("Cannot start a non-registered task.");

            if (!force && Timer != null)
                return Timer.Start(task);

            var queued = Queue(task);
            if (queued == null) return null;

            RunningTasks[queued.Id] = start ?? DateTime.Now;

            TaskStarted?.Invoke(queued);

            return queued;
        }

        /// <summary>
        /// Pauses the running task without removing it from the state.
        /// </summary>
        public void Pause(RunnableTask task, bool force = false)
        {
            if (!Contains(task.Id))
                throw new InvalidOperationException("Cannot pause a non-registered task.");

            if (!RunningTasks.ContainsKey(task.Id))
                throw new InvalidOperationException("That task hasn't been started yet!");

            if (!force && Timer != null)
            {
                Timer.Pause(task);
                return;
            }

            pushTimeFrame(task);

            TaskPaused?.Invoke(task);
        }

        /// <summary>
        /// Ends the currently running task.
        /// </summary>
        public void End(RunnableTask task, bool force = false, bool push = true)
        {
            if (!Contains(task.Id))
                throw new InvalidOperationException("Cannot end a non-registered task.");

            if (GetState(task) == TaskState.Stopped)
                throw new InvalidCastException("Cannot end a task when the task was not started.");

            if (!force && Timer != null)
            {
                Timer.End(task);
                return;
            }

            if (push) pushTimeFrame(task);

            RunningTasks.Remove(task.Id);
            if (task == FocusTask)
                FocusTask = null;

            if (push) TaskEnded?.Invoke(task);
        }

        /// <summary>
        /// Ends every running task.
        /// </summary>
        public void EndAll()
        {
            foreach (var (id, date) in RunningTasks)
            {
                if (date.HasValue)
                    End((RunnableTask)Find(id));
            }
        }

        /// <summary>
        /// Marks the <paramref name="task"/> as complete and ends it (if it is running).
        /// </summary>
        public void MarkComplete(RunnableTask task)
        {
            Modify(task, t =>
            {
                t.Complete = DateTime.Now;
            });

            if (GetState(task) != TaskState.Stopped)
                End(task);
        }

        /// <summary>
        /// Gets the state of the given <paramref name="task"/>
        /// </summary>
        public TaskState GetState(RunnableTask task)
        {
            var timerState = Timer?.GetState(task);

            if (timerState.HasValue)
                return timerState.Value;

            if (RunningTasks.TryGetValue(task.Id, out DateTime? dt))
                return dt != null ? TaskState.Running : TaskState.Paused;

            return TaskState.Stopped;
        }

        /// <summary>
        /// Returns the start time of a task, or null if it is paused/not running.
        /// </summary>
        public DateTime? GetStart(RunnableTask task)
        {
            bool found = RunningTasks.TryGetValue(task.Id, out DateTime? val);

            return found ? val : null;
        }

        /// <summary>
        /// Gets the string representation of the running time of the given <paramref name="task"/>.
        /// If a <see cref="Timer"/> is supplied, it will generate the current time.
        /// </summary>
        public string GetTime(RunnableTask task = null)
        {
            task ??= FocusTask;

            if (task == null)
                return "<no task>";

            if (Timer != null)
                return Timer.GetTime(task);

            if (RunningTasks.TryGetValue(task.Id, out DateTime? start) && start.HasValue)
                return (DateTime.Now - start.Value).ToString("G");

            return null;
        }

        protected override void Update()
        {
            base.Update();

            Timer?.Update();
        }

        /// <summary>
        /// Ends the timer and saves the time frame to the currently running task.
        /// </summary>
        private void pushTimeFrame(RunnableTask task)
        {
            if (!RunningTasks.ContainsKey(task.Id))
                return;

            var startDate = RunningTasks[task.Id];

            if (startDate == null)
                return;

            Modify(task, t =>
            {
                task.WorkTime.Add(new TimeFrame
                {
                    Start = startDate.Value,
                    End = DateTime.Now
                });
            });

            RunningTasks[task.Id] = null;
        }
    }

    public enum TaskState
    {
        Stopped,
        Paused,
        Running
    }
}
