﻿using System.Collections.Generic;
using timetable.Game.IO;
using timetable.Game.Tasks.Types;

namespace timetable.Game.Tasks.Stores
{
    public class GroupStore : GuidObjectStore<Group>
    {
        private readonly JsonStore<StoreObject> store;

        public override List<Group> Items => store.Value.Groups;

        public GroupStore(JsonStore<StoreObject> store)
        {
            this.store = store;
        }
    }
}
