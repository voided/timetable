﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using osu.Framework.Graphics;

namespace timetable.Game.Tasks.Stores
{
    public abstract class GuidObjectStore<T> : Component
        where T : GuidObject
    {
        public abstract List<T> Items { get; }

        public event Action<T> ItemCreated;
        public event Action<T> ItemUpdated;
        public event Action<T> ItemDeleted;

        /// <summary>
        /// Finds an item based on its id.
        /// </summary>
        public T Find(Guid id) => Items.Find(t => t.Id == id);

        /// <summary>
        /// Determines whether this store contains an item with the given <paramref name="id"/>
        /// </summary>
        public bool Contains(Guid id) => Find(id) != null;

        /// <summary>
        /// Adds an item to the store.
        /// </summary>
        public void Add(T item)
        {
            if (Contains(item.Id))
                throw new InvalidOperationException("Cannot add an item that has already been registered.");

            item.Created = DateTime.Now;

            Items.Add(item);
            Save();

            ItemCreated?.Invoke(item);
        }

        /// <summary>
        /// Modifies a task using the given function,
        /// and register it if it doesn't exist.
        /// </summary>
        public void Modify<TType>(TType item, Action<TType> modify)
            where TType : T
        {
            var retTask = Find(item.Id);

            if (retTask == null)
            {
                modify(item);
                Add(item);
            }
            else
            {
                modify(retTask as TType);
                Save();

                ItemUpdated?.Invoke(item);
            }
        }

        /// <summary>
        /// Clears all items from the store.
        /// </summary>
        public void Clear()
        {
            foreach (var item in Items.ToArray()) // avoid collection modified error
                Delete(item);
        }

        /// <summary>
        /// Deletes an item from this store.
        /// </summary>
        public void Delete(T item)
        {
            if (!Contains(item.Id))
                throw new InvalidOperationException("Cannot delete an item that isn't registered!");

            PrepareDeletion(item);

            var removed = Items.RemoveAll(t => t.Id == item.Id);
            Trace.Assert(removed == 1);

            Save();

            ItemDeleted?.Invoke(item);
        }

        /// <summary>
        /// Prepares the given <paramref name="item"/> for deletion.
        /// Run before <see cref="ItemDeleted"/> is fired.
        /// </summary>
        protected virtual void PrepareDeletion(T item)
        {
        }

        protected virtual void Save()
        {
        }
    }
}
