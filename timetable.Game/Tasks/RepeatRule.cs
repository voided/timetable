﻿using System;
using System.Diagnostics;
using Newtonsoft.Json;

namespace timetable.Game.Tasks
{
    [JsonObject(MemberSerialization.OptIn)]
    public struct RepeatRule
    {
        private const string date_format = @"yyyy/MM/dd (dddd)";

        [JsonProperty("startDate")]
        public DateTime StartDate { get; }

        [JsonProperty("type")]
        public RepeatType Type { get; }

        [JsonProperty("interval")]
        public TimeSpan? Interval { get; }

        [JsonProperty("days")]
        public Days? Days { get; }

        public RepeatRule(DateTime startDate, RepeatType type, TimeSpan? interval = null, Days? days = null)
        {
            StartDate = startDate;
            Type = type;
            Interval = interval;
            Days = days;

            switch (type)
            {
                case RepeatType.Interval:
                    if (!Interval.HasValue) throw new Exception($"No value for {nameof(Interval)} when one is required");

                    break;

                case RepeatType.OnDays:
                    if (!Days.HasValue) throw new Exception($"No value for {Days} when one is required");

                    break;

                default:
                    throw new Exception("Invalid repeat rule type");
            }
        }

        public RepeatRule Copy() => new RepeatRule(StartDate, Type, Interval, Days);

        /// <summary>
        /// Checks whether <paramref name="date"/> is valid on this rule
        /// (i.e. whether this task should repeat on that day)
        /// </summary>
        public bool IntervalValid(DateTime date)
        {
            if (Type != RepeatType.Interval)
                throw new Exception("Cannot check interval validity for a non-interval rule!");

            Trace.Assert(Interval.HasValue);

            return Math.Floor((DateTime.Today - StartDate).TotalDays) % Math.Floor(Interval.Value.TotalDays) == 0;
        }

        /// <summary>
        /// Checks whether <paramref name="date"/> is valid on this rule
        /// (i.e. whether this task should repeat on that day)
        /// </summary>
        public bool DaysValid(DateTime date)
        {
            if (Type != RepeatType.OnDays)
                throw new Exception("Cannot check days validity for a non-days rule!");

            Trace.Assert(Days.HasValue);

            var days = Days.Value;

            switch (date.DayOfWeek)
            {
                case DayOfWeek.Monday:
                    if (!days.HasFlag(Tasks.Days.Monday))
                        return false;

                    break;

                case DayOfWeek.Tuesday:
                    if (!days.HasFlag(Tasks.Days.Tuesday))
                        return false;

                    break;

                case DayOfWeek.Wednesday:
                    if (!days.HasFlag(Tasks.Days.Wednesday))
                        return false;

                    break;

                case DayOfWeek.Thursday:
                    if (!days.HasFlag(Tasks.Days.Thursday))
                        return false;

                    break;

                case DayOfWeek.Friday:
                    if (!days.HasFlag(Tasks.Days.Friday))
                        return false;

                    break;

                case DayOfWeek.Saturday:
                    if (!days.HasFlag(Tasks.Days.Saturday))
                        return false;

                    break;

                case DayOfWeek.Sunday:
                    if (!days.HasFlag(Tasks.Days.Sunday))
                        return false;

                    break;
            }

            return true;
        }

        public override string ToString() => ToString(false);

        public string ToString(bool isShort)
        {
            var result = isShort ? "Repeats " : $"Starting {StartDate.ToString(date_format)}, repeats ";

            switch (Type)
            {
                case RepeatType.Interval:
                    Trace.Assert(Interval.HasValue);
                    result += $"every {Interval.Value.TotalDays} days";
                    break;

                case RepeatType.OnDays:
                    Trace.Assert(Days.HasValue);
                    result += Days == Tasks.Days.None ? "never" : $"on {Days.Value.ToWeekString()}";
                    break;
            }

            return result;
        }
    }

    public enum RepeatType
    {
        Interval,
        OnDays
    }
}
