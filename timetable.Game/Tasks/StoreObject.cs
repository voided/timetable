﻿using System.Collections.Generic;
using Newtonsoft.Json;
using timetable.Game.Tasks.Types;

namespace timetable.Game.Tasks
{
    [JsonObject(MemberSerialization.OptIn)]
    public class StoreObject
    {
        /// <summary>
        /// Version of the object. Should only be incremented if a migration is necessary.
        /// </summary>
        [JsonProperty("version")]
        public int Version => 1;

        [JsonProperty("groups")]
        public List<Group> Groups { get; set; } = new List<Group>();

        [JsonProperty("tasks")]
        public List<Task> Tasks { get; set; } = new List<Task>();
    }
}
