﻿namespace timetable.Game.Tasks.Types
{
    public interface IHasRepeatRule
    {
        RepeatRule RepeatRule { get; set; }
    }
}
