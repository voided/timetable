﻿using Newtonsoft.Json;
using timetable.Game.Tasks.Stores;

namespace timetable.Game.Tasks.Types
{
    [JsonObject(MemberSerialization.OptIn)]
    public class Group : GuidObject
    {
        [JsonProperty("name")]
        public string Name { get; set; }
    }
}
