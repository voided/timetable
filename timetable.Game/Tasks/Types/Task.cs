﻿using System;
using Newtonsoft.Json;
using timetable.Game.Tasks.Stores;

namespace timetable.Game.Tasks.Types
{
    [JsonObject(MemberSerialization.OptIn)]
    public abstract class Task : GuidObject, IComparable<Task>
    {
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("group")]
        public string Group { get; set; }

        [JsonProperty("desc")]
        public string Description { get; set; }

        [JsonProperty("priority")]
        public int Priority { get; set; }

        public virtual int CompareTo(Task other) => other.Priority.CompareTo(Priority);
    }
}
