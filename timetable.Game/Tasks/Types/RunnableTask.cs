﻿using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;

namespace timetable.Game.Tasks.Types
{
    [JsonObject(MemberSerialization.OptIn)]
    public abstract class RunnableTask : Task
    {
        [JsonProperty("complete")]
        public DateTime? Complete { get; set; }

        [JsonProperty("workTime")]
        public List<TimeFrame> WorkTime { get; set; } = new List<TimeFrame>();

        public TimeSpan WorkDuration =>
            WorkTime.Select(tf => tf.End - tf.Start)
                    .Aggregate(TimeSpan.Zero, (acc, curr) => acc + curr);
    }
}
