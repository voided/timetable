﻿using System;
using Newtonsoft.Json;

namespace timetable.Game.Tasks.Types
{
    public class RepeatableTask : Task, IHasRepeatRule
    {
        [JsonProperty("repeatRule")]
        public RepeatRule RepeatRule { get; set; }

        public RepeatChildTask CreateRepeatChild()
        {
            if (Id == null)
                throw new Exception("Tried to create a repeat child of a non registered task!");

            return new RepeatChildTask
            {
                Name = Name,
                Description = Description,
                Priority = Priority,
                Parent = Id,
                RepeatRule = RepeatRule.Copy()
            };
        }
    }
}
