﻿using System;
using Newtonsoft.Json;

namespace timetable.Game.Tasks.Types
{
    [JsonObject(MemberSerialization.OptIn)]
    public class GeneralTask : RunnableTask
    {
        [JsonProperty("due")]
        public DateTime? Due { get; set; }

        public override int CompareTo(Task other)
        {
            if (Due.HasValue && other is GeneralTask gt && gt.Due.HasValue)
                return Due.Value.CompareTo(gt.Due.Value);

            return base.CompareTo(other);
        }
    }
}
