﻿using System;
using Newtonsoft.Json;

namespace timetable.Game.Tasks.Types
{
    [JsonObject(MemberSerialization.OptIn)]
    public class RepeatChildTask : RunnableTask, IHasRepeatRule
    {
        [JsonProperty("parent")]
        public Guid Parent { get; set; }

        [JsonProperty("repeatRule")]
        public RepeatRule RepeatRule { get; set; }
    }
}
