﻿using System;

namespace timetable.Game.Tasks
{
    [Flags]
    public enum Days
    {
        None = 0,
        Monday = 1,
        Tuesday = 1 << 1,
        Wednesday = 1 << 2,
        Thursday = 1 << 3,
        Friday = 1 << 4,
        Saturday = 1 << 5,
        Sunday = 1 << 6
    }

    public static class DaysExtensions
    {
        public static string ToWeekString(this Days days)
        {
            var result = "";

            if (days.HasFlag(Days.Monday))
                result += 'M';
            if (days.HasFlag(Days.Tuesday))
                result += "Tu";
            if (days.HasFlag(Days.Wednesday))
                result += 'W';
            if (days.HasFlag(Days.Thursday))
                result += "Th";
            if (days.HasFlag(Days.Friday))
                result += 'F';
            if (days.HasFlag(Days.Saturday))
                result += "Sa";
            if (days.HasFlag(Days.Sunday))
                result += "Su";

            return result;
        }
    }
}
