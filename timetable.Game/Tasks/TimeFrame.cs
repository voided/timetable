﻿using System;
using Newtonsoft.Json;

namespace timetable.Game.Tasks
{
    public class TimeFrame
    {
        [JsonProperty("start")]
        public DateTime Start { get; set; }

        [JsonProperty("end")]
        public DateTime End { get; set; }
    }
}
