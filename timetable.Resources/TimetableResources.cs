﻿using System.Reflection;

namespace timetable.Resources
{
    public static class TimetableResources
    {
        public static Assembly ResourceAssembly => typeof(TimetableResources).Assembly;
    }
}
