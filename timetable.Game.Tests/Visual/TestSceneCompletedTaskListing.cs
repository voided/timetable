﻿using System;
using System.Collections.Generic;
using osu.Framework.Graphics;
using osu.Framework.Testing;
using timetable.Game.Tasks;
using timetable.Game.Tasks.Types;
using timetable.Game.UI.Tasks;

namespace timetable.Game.Tests.Visual
{
    public class TestSceneCompletedTaskListing : TestScene
    {
        public TestSceneCompletedTaskListing()
        {
            CompletedTaskListing listing;

            Add(listing = new CompletedTaskListing { RelativeSizeAxes = Axes.Both });

            for (int i = 0; i < 5; i++)
            {
                var workTime = new List<TimeFrame>();

                for (int j = 0; j < i + 1; j++)
                {
                    var day = DateTime.Today.AddDays(-j);

                    workTime.Add(new TimeFrame
                    {
                        Start = day,
                        End = day.AddMinutes(10)
                    });
                }

                listing.Add(new GeneralTask
                {
                    Name = "Test" + i,
                    WorkTime = workTime,
                    Complete = DateTime.Today
                });
            }
        }
    }
}
