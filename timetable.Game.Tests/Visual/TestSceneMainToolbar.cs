﻿using NUnit.Framework;
using osu.Framework.Allocation;
using osu.Framework.Testing;
using timetable.Game.Screens.Main;

namespace timetable.Game.Tests.Visual
{
    public class TestSceneMainToolbar : TestScene
    {
        private MainToolbar toolbar;

        [BackgroundDependencyLoader]
        private void load()
        {
            Add(toolbar = new MainToolbar());
        }

        [Test]
        public void TestDragging()
        {
            AddStep("dragging", () =>
            {
                toolbar.SetDragging(true);
            });
        }

        [Test]
        public void TestNotDragging()
        {
            AddStep("not dragging", () =>
            {
                toolbar.SetDragging(false);
            });
        }
    }
}
