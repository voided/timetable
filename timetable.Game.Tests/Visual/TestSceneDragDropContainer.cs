﻿using System;
using osu.Framework.Graphics;
using osu.Framework.Graphics.Shapes;
using osu.Framework.Graphics.Sprites;
using osu.Framework.Testing;
using osuTK;
using osuTK.Graphics;
using timetable.Game.UI;

namespace timetable.Game.Tests.Visual
{
    public class TestSceneDragDropContainer : TestScene
    {
        public TestSceneDragDropContainer()
        {
            FillFlowDropContainer<TestDragDropItem, TestItemInfo> flow1;
            FillFlowDropContainer<TestDragDropItem, TestItemInfo> flow2;
            TestDragDropContainer container;

            Add(container = new TestDragDropContainer
            {
                RelativeSizeAxes = Axes.Both,
                Children = new Drawable[]
                {
                    flow1 = new FillFlowDropContainer<TestDragDropItem, TestItemInfo>
                    {
                        RelativeSizeAxes = Axes.Both,
                        Width = 0.5f,
                        Spacing = new Vector2(2)
                    },
                    flow2 = new FillFlowDropContainer<TestDragDropItem, TestItemInfo>
                    {
                        RelativeSizeAxes = Axes.Both,
                        Width = 0.5f,
                        Anchor = Anchor.TopRight,
                        Origin = Anchor.TopRight
                    }
                }
            });

            const int items = 30;

            for (int i = 1; i <= items; i++)
            {
                var hue = (float)i / items;

                flow1.Add(new TestItemInfo
                {
                    Color = Colour4.FromHSV(hue, 1, 0.7f),
                    Number = i
                });
            }

            AddStep("automatic drag", () =>
            {
                var item = flow1.Count > 0 ? flow1[0] : flow2[0];

                container.DragTo(item, flow1.Count > 0 ? flow2 : flow1);
            });
        }

        private class TestItemInfo : IComparable<TestItemInfo>
        {
            public int Number { get; init; }

            public Color4 Color { get; init; }

            public Vector2 Size => new((Number % 3 + 1) * 75, 50);

            public string Text => $"test {Number}";

            public int CompareTo(TestItemInfo other)
            {
                return Number.CompareTo(other.Number);
            }
        }

        private class TestDragDropContainer : DragDropContainer<TestItemInfo>
        {
            protected override DragDropItem<TestItemInfo> CreateCursor() => new TestDragDropItem();
        }

        private class TestDragDropItem : DragDropItem<TestItemInfo>
        {
            private TestItemInfo model;

            public override TestItemInfo Model
            {
                get => model;
                set
                {
                    model = value;

                    if (model == null)
                        return;

                    background.Colour = model.Color;
                    text.Text = model.Text;
                    Size = model.Size;
                }
            }

            private readonly Box background;
            private readonly SpriteText text;

            public TestDragDropItem()
            {
                InternalChildren = new Drawable[]
                {
                    background = new Box
                    {
                        RelativeSizeAxes = Axes.Both
                    },
                    text = new SpriteText
                    {
                        Font = FontUsage.Default.With(size: 25),
                        Colour = Color4.White,
                        ShadowColour = Color4.Black,
                        Shadow = true
                    }
                };
            }
        }
    }
}
