﻿using System;
using System.Collections.Generic;
using osu.Framework.Graphics;
using osu.Framework.Graphics.Containers;
using osu.Framework.Testing;
using timetable.Game.UI.Forms;
using timetable.Game.UI.Forms.Fields;

namespace timetable.Game.Tests.Visual
{
    public class TestSceneForms : TestScene
    {
        public TestSceneForms()
        {
            var fields = new List<IEditableField>();

            EditableTextField field1;
            EditableDateField field2;
            EditableRepeatRuleField field3;
            EditorDateBox date;

            Add(new FillFlowContainer
            {
                RelativeSizeAxes = Axes.Both,
                LayoutEasing = Easing.OutQuad,
                LayoutDuration = 200,
                Children = new Drawable[]
                {
                    new EditorTextBox
                    {
                        Width = 500,
                        PlaceholderText = "Placeholder"
                    },
                    field1 = new EditableTextField("TALL", new EditorTextBox
                    {
                        Height = 50
                    })
                    {
                        Width = 200
                    },
                    field2 = new EditableDateField("Date", date = new EditorDateBox())
                    {
                        Width = 500
                    },
                    field3 = new EditableRepeatRuleField("Repeat Rule", new EditorRepeatRuleBox())
                    {
                        Width = 600
                    }
                },
                Direction = FillDirection.Vertical
            });

            fields.Add(field1);
            fields.Add(field2);
            fields.Add(field3);

            AddStep("toggle field editable", () => fields.ForEach(f => f.Editable = !f.Editable));
            AddStep("set date to now", () => date.Current.Value = DateTime.Now);
            AddStep("toggle AllowNull", () => date.DisplayClearCheckbox = !date.DisplayClearCheckbox);
        }
    }
}
