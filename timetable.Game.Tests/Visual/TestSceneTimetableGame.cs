using osu.Framework.Allocation;
using osu.Framework.Testing;

namespace timetable.Game.Tests.Visual
{
    public class TestSceneTimetableGame : TestScene
    {
        [BackgroundDependencyLoader]
        private void load()
        {
            AddGame(new TimetableGame());
        }
    }
}
