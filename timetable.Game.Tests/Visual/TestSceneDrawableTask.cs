﻿using System;
using osu.Framework.Testing;
using timetable.Game.Tasks.Types;
using timetable.Game.UI.Tasks;

namespace timetable.Game.Tests.Visual
{
    public class TestSceneDrawableTask : TestScene
    {
        public TestSceneDrawableTask()
        {
            DrawableTask dt;

            Add(dt = new DrawableTask
            {
                Model = new GeneralTask
                {
                    Name = "task task task task task productive long task task task task task task task task task task task task task task task task task task task",
                    Group = "group group group AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA",
                    Due = DateTime.Now
                }
            });

            AddStep("toggle expand", () => dt.Expanded = !dt.Expanded);
        }
    }
}
