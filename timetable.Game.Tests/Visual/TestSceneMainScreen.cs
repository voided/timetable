using osu.Framework.Graphics;
using osu.Framework.Screens;
using timetable.Game.Screens;
using timetable.Game.Tests.Visual.TaskStore;

namespace timetable.Game.Tests.Visual
{
    public class TestSceneMainScreen : TaskStoreTestScene
    {
        // Add visual tests to ensure correct behaviour of your game: https://github.com/ppy/osu-framework/wiki/Development-and-Testing
        // You can make changes to classes associated with the tests and they will recompile and update immediately.

        protected override void LoadComplete()
        {
            base.LoadComplete();

            Add(new ScreenStack(new MainScreen()) { RelativeSizeAxes = Axes.Both });
        }
    }
}
