﻿using System;
using System.Collections.Generic;
using osu.Framework.Allocation;
using osu.Framework.Testing;
using timetable.Game.IO;
using timetable.Game.Tasks;

namespace timetable.Game.Tests.Visual.TaskStore
{
    [ExcludeFromDynamicCompile]
    public abstract class TaskStoreTestScene : TestScene
    {
        protected readonly TestTaskStore TaskStore;

        protected TaskStoreTestScene()
        {
            TaskStore = new TestTaskStore
            {
                Timer = null
            };
        }

        private DependencyContainer dependencies;

        protected override IReadOnlyDependencyContainer CreateChildDependencies(IReadOnlyDependencyContainer parent)
            => dependencies = new DependencyContainer(base.CreateChildDependencies(parent));

        protected override void LoadComplete()
        {
            base.LoadComplete();

            dependencies.CacheAs<Tasks.Stores.TaskStore>(TaskStore);
        }

        protected class TestTaskStore : Tasks.Stores.TaskStore
        {
            public new Dictionary<Guid, DateTime?> RunningTasks => base.RunningTasks;

            public TestTaskStore()
                : base(new MemoryJsonStore())
            {
            }
        }

        private class MemoryJsonStore : JsonStore<StoreObject>
        {
            private string text;

            public MemoryJsonStore()
            {
                Load();
            }

            protected override string GetSerialized() => text;

            public override void Save() => text = Serialize();
        }
    }
}
