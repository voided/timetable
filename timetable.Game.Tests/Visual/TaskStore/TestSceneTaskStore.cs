﻿using System;
using System.Collections.Generic;
using System.Linq;
using kyoseki.UI.Components;
using NUnit.Framework;
using osu.Framework.Graphics;
using osu.Framework.Graphics.Containers;
using osu.Framework.Graphics.Shapes;
using osu.Framework.Graphics.Sprites;
using osu.Framework.Testing;
using osuTK;
using timetable.Game.Tasks;
using timetable.Game.Tasks.Types;

namespace timetable.Game.Tests.Visual.TaskStore
{
    public class TestSceneTaskStore : TaskStoreTestScene
    {
        private readonly FillFlowContainer<DrawableDebugTask> taskFlow;

        public TestSceneTaskStore()
        {
            Add(new KyosekiScrollContainer
            {
                RelativeSizeAxes = Axes.Both,
                Child = taskFlow = new FillFlowContainer<DrawableDebugTask>
                {
                    RelativeSizeAxes = Axes.X,
                    AutoSizeAxes = Axes.Y,
                    Spacing = new Vector2(0, 5)
                }
            });

            TaskStore.ItemCreated += addTask;
        }

        [SetUpSteps]
        public void SetUpSteps()
        {
            AddStep("clear tasks", TaskStore.Clear);
        }

        [Test]
        public void TestCreateDeleteTask()
        {
            Task t = null;

            AddStep("create a task", () =>
            {
                TaskStore.Add(t = new GeneralTask
                {
                    Name = "test task creation",
                    Group = "test"
                });
            });

            assertTaskExists(() => t);

            AddStep("start the task", () => TaskStore.Start(t));

            AddStep("delete the task", () =>
            {
                TaskStore.Delete(t);
            });

            assertTaskExists(() => t, false);

            AddAssert("task stopped", () => TaskStore.RunningTasks.Count == 0);
        }

        [Test]
        public void TestModifyTask()
        {
            Task t = null;

            AddStep("create a task", () =>
            {
                TaskStore.Add(t = new GeneralTask
                {
                    Name = "before modify",
                    Group = "test"
                });
            });

            assertTaskExists(() => t);

            const string after_modify = "after modify";

            AddStep("update task", () =>
            {
                TaskStore.Modify(t, ts =>
                {
                    ts.Name = after_modify;
                });
            });

            (Task, DrawableDebugTask) taskData = (null, null);

            AddStep("fetch tasks", () => taskData = getTask(t));

            AddAssert("check task updated", () => taskData.Item1.Name == after_modify);

            AddAssert("check event propagated", () => taskData.Item2.Updates == 1);
        }

        [Test]
        public void TestTaskTiming()
        {
            const int task_count = 5;

            var tasks = new List<RunnableTask>();

            AddStep("create tasks", () =>
            {
                tasks.Clear();

                for (int i = 0; i < task_count; i++)
                {
                    GeneralTask t;

                    TaskStore.Add(t = new GeneralTask
                    {
                        Name = $"test {i + 1}",
                        Group = "test"
                    });

                    tasks.Add(t);
                }
            });

            int j = 0;

            AddStep("reset counter", () => j = 0);

            AddRepeatStep("start tasks", () =>
            {
                TaskStore.Start(tasks[j]);
                j++;
            }, task_count);

            AddAssert("check tasks running", () => TaskStore.RunningTasks.Count == task_count);

            int k = 0;

            AddStep("reset counter", () => k = task_count - 1);

            AddRepeatStep("pause tasks", () =>
            {
                TaskStore.Pause(tasks[k]);
                k--;
            }, task_count);

            AddAssert("compare task timing", () =>
            {
                for (int i = 0; i < task_count - 1; i++)
                {
                    if (tasks[i].WorkDuration < tasks[i + 1].WorkDuration)
                        return false;
                }

                return true;
            });

            AddStep("end tasks", () =>
            {
                foreach (var task in tasks)
                {
                    TaskStore.End(task);
                }
            });

            AddAssert("all tasks ended", () => TaskStore.RunningTasks.Count == 0);
        }

        [Test]
        public void TestRepeatTasksCreateChild()
        {
            var interval = new TimeSpan(5, 0, 0, 0);
            DateTime startValid = DateTime.Today;
            DateTime startInvalid = DateTime.Today;

            AddStep("find dates to use", () =>
            {
                startValid = DateTime.Today - interval;
                startInvalid = startValid.AddDays(1);
            });

            Task intervalP1 = null;
            Task intervalP2 = null;

            AddStep("create tasks (interval)", () =>
            {
                // create one rule that should repeat today (valid)
                TaskStore.Add(intervalP1 = new RepeatableTask
                {
                    Name = "repeatable interval (valid today)",
                    Group = "test",
                    RepeatRule = new RepeatRule(startValid, RepeatType.Interval, interval)
                });

                // and one that should repeat tomorrow (invalid)
                TaskStore.Add(intervalP2 = new RepeatableTask
                {
                    Name = "repeatable interval (invalid today)",
                    Group = "test",
                    RepeatRule = new RepeatRule(startInvalid, RepeatType.Interval, interval)
                });
            });

            AddAssert("interval task 1 is valid", () => TaskStore.Start(intervalP1) != null);
            AddAssert("interval task 2 is invalid", () => TaskStore.Start(intervalP2) == null);

            Days validDay = Days.None;
            Days invalidDay = Days.None;

            AddStep("find days to use", () =>
            {
                switch (DateTime.Today.DayOfWeek)
                {
                    case DayOfWeek.Monday:
                        validDay = Days.Monday;
                        invalidDay = Days.Tuesday;
                        break;

                    case DayOfWeek.Tuesday:
                        validDay = Days.Tuesday;
                        invalidDay = Days.Wednesday;
                        break;

                    case DayOfWeek.Wednesday:
                        validDay = Days.Wednesday;
                        invalidDay = Days.Thursday;
                        break;

                    case DayOfWeek.Thursday:
                        validDay = Days.Thursday;
                        invalidDay = Days.Friday;
                        break;

                    case DayOfWeek.Friday:
                        validDay = Days.Friday;
                        invalidDay = Days.Saturday;
                        break;

                    case DayOfWeek.Saturday:
                        validDay = Days.Saturday;
                        invalidDay = Days.Sunday;
                        break;

                    case DayOfWeek.Sunday:
                        validDay = Days.Sunday;
                        invalidDay = Days.Monday;
                        break;

                    default:
                        throw new Exception("tf?");
                }
            });

            Task daysP1 = null;
            Task daysP2 = null;

            AddStep("create tasks (days)", () =>
            {
                // create one rule that repeats on this week day (valid)
                TaskStore.Add(daysP1 = new RepeatableTask
                {
                    Name = "repeatable days (valid today)",
                    Group = "test",
                    RepeatRule = new RepeatRule(DateTime.Today, RepeatType.OnDays, days: validDay)
                });

                // and another that repeats tomorrow (invalid)
                TaskStore.Add(daysP2 = new RepeatableTask
                {
                    Name = "repeatable days (invalid today)",
                    Group = "test",
                    RepeatRule = new RepeatRule(DateTime.Today, RepeatType.OnDays, days: invalidDay)
                });
            });

            AddAssert("days task 1 is valid", () => TaskStore.Start(daysP1) != null);
            AddAssert("days task 2 is invalid", () => TaskStore.Start(daysP2) == null);
        }

        [Test]
        public void TestRepeatTasksStartChild()
        {
            RepeatableTask intervalParent = null;
            RepeatChildTask intervalRct = null;

            AddStep("create tasks (interval)", () =>
            {
                var interval = new TimeSpan(3, 0, 0, 0);
                var startDate = DateTime.Today - interval;

                TaskStore.Add(intervalParent = new RepeatableTask
                {
                    Name = "repeatable interval",
                    Group = "test",
                    RepeatRule = new RepeatRule(startDate, RepeatType.Interval, interval)
                });

                TaskStore.Add(intervalRct = intervalParent.CreateRepeatChild());
                TaskStore.Modify(intervalRct, t =>
                {
                    t.Created = DateTime.Now;
                });
            });

            RepeatableTask daysParent = null;
            RepeatChildTask daysRct = null;

            AddStep("create tasks (days)", () =>
            {
                TaskStore.Add(daysParent = new RepeatableTask
                {
                    Name = "repeatable days",
                    Group = "test",
                    RepeatRule = new RepeatRule(DateTime.Today, RepeatType.OnDays, days: Days.Monday | Days.Wednesday | Days.Friday)
                });

                TaskStore.Add(daysRct = daysParent.CreateRepeatChild());
                TaskStore.Modify(daysRct, t =>
                {
                    t.Created = DateTime.Today; // check is only for same day, doesn't really matter which (should be handled by creation)
                });
            });

            Task startedInterval = null;

            AddStep("start interval parent", () => startedInterval = TaskStore.Start(intervalParent));

            AddAssert("started interval child", () => startedInterval == intervalRct);

            Task startedDays = null;

            AddStep("start days parent", () => startedDays = TaskStore.Start(daysParent));

            AddAssert("started days child", () => startedDays == daysRct);
        }

        private void addTask(Task t) => taskFlow.Add(new DrawableDebugTask(TaskStore, t)
        {
            RelativeSizeAxes = Axes.X,
            Height = 50
        });

        private (Task, DrawableDebugTask) getTask(Task t)
        {
            var res = TaskStore.Find(t.Id);
            var resDrawable = taskFlow.Children.FirstOrDefault(d => d.Task.Id == t.Id);

            return (res, resDrawable);
        }

        private void assertTaskExists(Func<Task> tFunc, bool exists = true)
        {
            var state = exists ? "exists" : "does not exist";

            AddAssert($"ensure task {state}", () =>
            {
                var (res, resDrawable) = getTask(tFunc());

                // test that the event propagated and that the task store saved it

                if (exists)
                    return res != null && resDrawable != null;

                return res == null && resDrawable == null;
            });
        }

        protected override void Dispose(bool isDisposing)
        {
            TaskStore.ItemCreated -= addTask;

            base.Dispose(isDisposing);
        }

        private class DrawableDebugTask : CompositeDrawable
        {
            private const int transition_duration = 150;

            public int Updates;

            private static Colour4 defaultBg => Colour4.Gray;
            private static Colour4 runningBg => Colour4.Lime;
            private static Colour4 pausedBg => Colour4.Yellow;
            private static Colour4 updatedBg => Colour4.Blue;

            private readonly Tasks.Stores.TaskStore store;
            public readonly Task Task;

            private readonly Box background;
            private readonly SpriteText text;

            public DrawableDebugTask(Tasks.Stores.TaskStore store, Task task)
            {
                this.store = store;
                Task = task;

                InternalChildren = new Drawable[]
                {
                    background = new Box
                    {
                        RelativeSizeAxes = Axes.Both,
                        Colour = defaultBg
                    },
                    text = new ThemedText
                    {
                        Colour = Colour4.White,
                        Anchor = Anchor.CentreLeft,
                        Origin = Anchor.CentreLeft,
                        Padding = new MarginPadding { Left = 25 }
                    },
                    new Box
                    {
                        RelativeSizeAxes = Axes.Y,
                        Colour = task is RepeatChildTask ? Colour4.Orange : Colour4.Black,
                        Width = 5,
                        Anchor = Anchor.CentreRight,
                        Origin = Anchor.CentreRight
                    }
                };

                store.TaskQueued += onStart;
                store.TaskPaused += onPause;
                store.TaskEnded += onEnd;
                store.ItemUpdated += onUpdate;
                store.ItemDeleted += onDelete;
            }

            private void onStart(Task t)
            {
                if (t != Task) return;

                background.FadeColour(runningBg, transition_duration);
            }

            private void onPause(Task t)
            {
                if (t != Task) return;

                background.FadeColour(pausedBg, transition_duration);
            }

            private void onEnd(Task t)
            {
                if (t != Task) return;

                background.FadeColour(defaultBg, transition_duration);
            }

            private void onUpdate(Task t)
            {
                if (t != Task) return;

                background.FlashColour(updatedBg, transition_duration);
                Updates++;
            }

            private void onDelete(Task t)
            {
                if (t != Task) return;

                this.FadeOut(transition_duration).Expire();
            }

            protected override void Update()
            {
                base.Update();

                var timeText = Task is RunnableTask rut ? rut.WorkDuration.ToString() : "No time data";
                text.Text = $"{Task.Name} ({timeText})";
            }

            protected override void Dispose(bool isDisposing)
            {
                store.TaskQueued -= onStart;
                store.TaskPaused -= onPause;
                store.TaskEnded -= onEnd;
                store.ItemUpdated -= onUpdate;
                store.ItemDeleted -= onDelete;

                base.Dispose(isDisposing);
            }
        }
    }
}
