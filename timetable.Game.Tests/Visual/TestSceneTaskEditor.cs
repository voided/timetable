﻿using NUnit.Framework;
using osu.Framework.Testing;
using timetable.Game.UI.Overlays;

namespace timetable.Game.Tests.Visual
{
    public class TestSceneTaskEditor : TestScene
    {
        private readonly TaskEditor te;

        public TestSceneTaskEditor()
        {
            Add(te = new TaskEditor());
        }

        [Test]
        public void TestShow()
        {
            AddStep("show", te.Show);
        }

        [Test]
        public void TestHide()
        {
            AddStep("hide", te.Hide);
        }
    }
}
