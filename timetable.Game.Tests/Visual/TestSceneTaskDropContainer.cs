﻿using System;
using osu.Framework.Graphics;
using osu.Framework.Graphics.Containers;
using osu.Framework.Testing;
using timetable.Game.Tasks;
using timetable.Game.Tasks.Types;
using timetable.Game.UI;
using timetable.Game.UI.Tasks;
using timetable.Game.UI.Tasks.TaskBank;

namespace timetable.Game.Tests.Visual
{
    public class TestSceneTaskDropContainer : TestScene
    {
        public TestSceneTaskDropContainer()
        {
            TaskBank bank;

            Add(new TaskDragDropContainer
            {
                RelativeSizeAxes = Axes.Both,
                Child = new GridContainer
                {
                    RelativeSizeAxes = Axes.Both,
                    Content = new[]
                    {
                        new Drawable[]
                        {
                            bank = new TaskBank
                            {
                                Columns = new TaskBankColumn[]
                                {
                                    new RepeatTaskBankColumn(),
                                    new GeneralTaskBankColumn()
                                }
                            }
                        }
                    }
                }
            });

            var due = DateTime.Today;

            for (int i = 0; i < 5; i++)
            {
                bank.Add(new DrawableTask
                {
                    Model = new GeneralTask
                    {
                        Name = "Test" + i,
                        Group = "Group",
                        Due = due
                    }
                });

                due = due.AddDays(1);
            }

            bank.Add(new DrawableTask
            {
                Model = new RepeatableTask
                {
                    Name = "Test Repeat",
                    Group = "Group",
                    RepeatRule = new RepeatRule(DateTime.Today, RepeatType.OnDays, days: Days.Friday)
                }
            });
        }

        private class TaskDragDropContainer : DragDropContainer<Task>
        {
            protected override DragDropItem<Task> CreateCursor() => new DrawableTask();
        }
    }
}
