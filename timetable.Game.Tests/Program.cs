﻿using osu.Framework;
using osu.Framework.Platform;

namespace timetable.Game.Tests
{
    public static class Program
    {
        public static void Main()
        {
            using (GameHost host = Host.GetSuitableDesktopHost("visual-tests"))
            using (var game = new TimetableTestBrowser())
                host.Run(game);
        }
    }
}
